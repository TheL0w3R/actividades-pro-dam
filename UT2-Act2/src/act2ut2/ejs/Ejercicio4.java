package act2ut2.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 08/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio4 extends MenuOption {

    public Ejercicio4() {
        super("Ejercicio 4");
    }

    @Override
    public void run() {
        double med = 0;
        int greater = 0;
        for(int i = 0; i < 10; i++) {
            System.out.print("Escribe el " + (i+1) + "º salario: ");
            double num = sc.nextDouble();
            med += num;
            if(num > 1500)
                greater++;
        }

        System.out.println("La media de los 10 salarios es " + med/10 + " y " + greater + " son mayores que 1500.");
    }
}
