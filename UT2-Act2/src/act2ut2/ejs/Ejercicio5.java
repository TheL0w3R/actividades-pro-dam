package act2ut2.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 08/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio5 extends MenuOption {

    public Ejercicio5() {
        super("Ejercicio 5");
    }

    @Override
    public void run() {
        int greater = 0;
        for(int i = 0; i < 7; i++) {
            System.out.print("Introduce el " + (i+1) + "º número: ");
            int num = sc.nextInt();
            if(num > greater)
                greater = num;
        }

        System.out.println("El número mayor es: " + greater);
    }
}
