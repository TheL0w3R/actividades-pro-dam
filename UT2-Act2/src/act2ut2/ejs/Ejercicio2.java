package act2ut2.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 08/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio2 extends MenuOption {

    public Ejercicio2() {
        super("Ejercicio 2");
    }

    @Override
    public void run() {
        int sum = 0;
        while(true) {
            System.out.print("Escribe un número (0 para salir): ");
            int num = sc.nextInt();

            if(num == 0) {
                System.out.println("La suma total es: " + sum);
                break;
            } else {
                sum += num;
            }
        }
    }
}
