package act2ut2.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 08/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio1 extends MenuOption {

    public Ejercicio1() {
        super("Ejercicio 1");
    }

    @Override
    public void run() {
        for(int i = 0; true; i++) {
            System.out.print("Introduce un numero posiivo (negativo para salir): ");
            int num = sc.nextInt();
            if(num < 0) {
                System.out.println("Has introducido " + i + " números positivos.");
                break;
            }
        }
    }
}
