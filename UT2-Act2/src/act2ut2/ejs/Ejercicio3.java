package act2ut2.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 08/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio3 extends MenuOption {

    public Ejercicio3() {
        super("Ejercicio 3");
    }

    @Override
    public void run() {
        double med = 0;
        for(int i = 0; i < 5; i++) {
            System.out.print("Introduce el " + (i+1) + "º número: ");
            double num = sc.nextDouble();
            med += num;
        }

        System.out.println("La media de los 5 números es: " + med/5);
    }
}
