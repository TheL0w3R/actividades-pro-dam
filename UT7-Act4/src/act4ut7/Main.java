package act4ut7;

import act4ut7.options.AddClient;
import act4ut7.options.Login;
import com.thel0w3r.tlmenuframework.MenuBuilder;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        new BankManager();
        new MenuBuilder()
                .title("Bank System")
                .addOption(new AddClient())
                .addOption(new Login())
                .exitNum(9)
                .build()
                .start();
    }

}
