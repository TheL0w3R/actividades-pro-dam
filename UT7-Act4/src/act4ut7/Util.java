package act4ut7;

import java.util.Random;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class Util {

    private static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String digits = "0123456789";
    private static int accountID = 0;

    public static int consumeID() {
        int tmp = accountID;
        accountID++;
        return tmp;
    }

    private static String gen(int length, String sym) {
        if (length < 1) throw new IllegalArgumentException();
        Random rand = new Random();
        char[] buffer = new char[length];
        for(int i = 0; i < length; i++)
            buffer[i] = sym.charAt(rand.nextInt(length));

        return new String(buffer);
    }

    public static String genAccount() {
        StringBuilder sb = new StringBuilder();
        sb.append(gen(2, upper))
                .append(gen(2, digits))
                .append(" ")
                .append(gen(4, digits))
                .append(" ")
                .append(gen(4, digits))
                .append(" ")
                .append(gen(4, digits))
                .append(" ")
                .append(gen(4, digits))
                .append(" ")
                .append(gen(4, digits));

        return sb.toString();
    }

}
