package act4ut7;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class Client {

    private String nif;
    private String name;
    private int tlf;

    public Client(String nif, String name, int tlf) {
        this.nif = nif;
        this.name = name;
        this.tlf = tlf;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTlf() {
        return tlf;
    }

    public void setTlf(int tlf) {
        this.tlf = tlf;
    }
}
