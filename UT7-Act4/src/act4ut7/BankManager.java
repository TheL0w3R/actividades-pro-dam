package act4ut7;

import act4ut7.bankservices.BankAccount;
import act4ut7.bankservices.BankDeposit;
import act4ut7.options.*;
import com.thel0w3r.tlmenuframework.Menu;
import com.thel0w3r.tlmenuframework.MenuBuilder;
import com.thel0w3r.tlmenuframework.MenuOption;

import java.util.ArrayList;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class BankManager {

    private static BankManager instance;

    private ArrayList<Client> clients = new ArrayList<>();
    private ArrayList<BankService> services = new ArrayList<>();

    private Client currentlyLoggedIn;
    private BankService currentService;

    public BankManager() {
        instance = this;
        currentlyLoggedIn = null;
    }

    public static BankManager getInstance() {
        return instance;
    }

    public void addClient(Client c) {
        if(!clients.contains(c))
            clients.add(c);
    }

    public Client[] getClients() {
        return clients.toArray(new Client[]{});
    }

    public void login(Client c) {
        currentlyLoggedIn = c;
        new MenuBuilder()
                .title("Welcome, " + c.getName())
                .addOption(new CreateAccount())
                .addOption(new CreateDeposit())
                .addOption(new ManageServices())
                .exitOption("Log out")
                .exitNum(9)
                .build()
                .start();
        logout();
    }

    private void logout() {
        currentlyLoggedIn = null;
    }

    public boolean isLoggedIn() {
        return currentlyLoggedIn != null;
    }

    public Client getLoggedIn() {
        return currentlyLoggedIn;
    }

    public BankService getCurrentService() {
        return currentService;
    }

    public void manageService(BankService bs) {
        currentService = bs;
        if(bs instanceof BankAccount) {
            new MenuBuilder()
                    .title("Managing account: " + bs.getName())
                    .addOption(new ViewBalance())
                    .addOption(new AddCurrency())
                    .addOption(new WithdrawCurrency())
                    .exitOption("Close")
                    .exitNum(9)
                    .build()
                    .start();
        } else {
            new MenuBuilder()
                    .title("Managing deposit: " + bs.getName())
                    .addOption(new ViewBalance())
                    .addOption(new WithdrawCurrency())
                    .exitOption("Close")
                    .exitNum(9)
                    .build()
                    .start();
        }
        this.currentService = null;
    }

    public void addService(BankService bs) {
        this.services.add(bs);
    }

    public BankService[] getServicesForActive() {
        ArrayList<BankService> tmp = new ArrayList<>();
        for(BankService bs : services)
            if(bs.getOwner().equals(currentlyLoggedIn)) tmp.add(bs);
        return tmp.toArray(new BankService[]{});
    }

    public BankAccount[] getAccountsForActive() {
        ArrayList<BankAccount> tmp = new ArrayList<>();
        for(BankService bs : getServicesForActive())
            if(bs instanceof BankAccount) tmp.add((BankAccount) bs);
        return tmp.toArray(new BankAccount[]{});
    }

    public BankDeposit[] getDepositsForActive() {
        ArrayList<BankDeposit> tmp = new ArrayList<>();
        for(BankService bs : getServicesForActive())
            if(bs instanceof BankDeposit) tmp.add((BankDeposit) bs);
        return tmp.toArray(new BankDeposit[]{});
    }

    public void consumeDeposit(BankDeposit bd) {
        this.services.removeIf(x->x.equals(bd));
        this.currentService = null;
    }
}
