package act4ut7.options;

import act4ut7.BankManager;
import act4ut7.BankService;
import act4ut7.InputSystem;
import act4ut7.bankservices.BankAccount;
import act4ut7.bankservices.BankDeposit;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 28/05/2018.
 * All Rights Reserved.
 */
public class WithdrawCurrency extends MenuOption {

    public WithdrawCurrency() {
        super("Withdraw Currency");
    }

    @Override
    public void run() {
        BankService bs = BankManager.getInstance().getCurrentService();
        if(bs != null) {
            if(bs instanceof BankAccount) {
                boolean errors;
                double curr = 0D;
                do {
                    curr = InputSystem.readDouble(sc, "Enter the amount to withdraw: ");
                    errors = curr < 0;
                    if(errors)
                        System.out.println("That's not a valid amount");
                } while(errors);

                ((BankAccount)bs).withdraw(curr);
            } else
                System.out.println("Deposit consumed, you retired: " + ((BankDeposit)bs).withdraw());
        } else
            System.out.println("The service has expired or been removed!");
    }
}
