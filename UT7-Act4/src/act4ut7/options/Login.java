package act4ut7.options;

import act4ut7.BankManager;
import act4ut7.Client;
import act4ut7.InputSystem;
import com.thel0w3r.tlmenuframework.MenuBuilder;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class Login extends MenuOption {

    public Login() {
        super("Log in as client");
    }

    @Override
    public void run() {
        StringBuilder sb = new StringBuilder();
        Client[] clients = BankManager.getInstance().getClients();
        for(int i = 0; i < clients.length; i++)
            sb.append("  ").append(i+1).append(" - ").append(clients[i].getName()).append("\n");

        sb.append("  0 - Cancel\n\nSelect a valid client: ");
        int op = 0;
        boolean errors;
        do {
            System.out.print(sb);
            op = InputSystem.readInt(sc, "");
            if(op < 0 || op > clients.length) {
                errors = true;
                System.out.println("This is not a valid client!\n");
            } else
                errors = false;
        } while(errors);
        System.out.println();

        if(op != 0)
            BankManager.getInstance().login(clients[op - 1]);
    }
}
