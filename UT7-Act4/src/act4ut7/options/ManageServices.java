package act4ut7.options;

import act4ut7.BankManager;
import act4ut7.BankService;
import act4ut7.InputSystem;
import act4ut7.bankservices.BankAccount;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 27/05/2018.
 * All Rights Reserved.
 */
public class ManageServices extends MenuOption {

    public ManageServices() {
        super("Manage services");
    }

    @Override
    public void run() {
        StringBuilder sb = new StringBuilder("Available Services\n\n");
        BankService[] services = BankManager.getInstance().getServicesForActive();
        for(int i = 0; i < services.length; i++)
            sb.append(" ").append(i + 1).append(" - ").append(services[i].getName())
                    .append(services[i] instanceof BankAccount ? " (Account)" : " (Deposit)")
                    .append("\n");

        sb.append("\nSelect a service to manage: ");
        int op = 0;
        boolean errors;
        do {
            System.out.print(sb);
            op = InputSystem.readInt(sc, "");
            if(op < 0 || op > services.length) {
                errors = true;
                System.out.println("This is not a valid service!\n");
            } else
                errors = false;
        } while(errors);

        BankManager.getInstance().manageService(services[op - 1]);
    }
}
