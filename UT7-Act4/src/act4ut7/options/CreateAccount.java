package act4ut7.options;

import act4ut7.BankManager;
import act4ut7.InputSystem;
import act4ut7.factories.BankAccountFactory;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class CreateAccount extends MenuOption {

    public CreateAccount() {
        super("Create Account");
    }

    @Override
    public void run() {
        BankAccountFactory bfa = new BankAccountFactory();
        bfa.setName(InputSystem.readString(sc, "Enter the account name: "));
        bfa.setDesc(InputSystem.readString(sc, "Enter the account description: "));
        BankManager.getInstance().addService(bfa.build());
    }
}
