package act4ut7.options;

import act4ut7.BankManager;
import act4ut7.BankService;
import act4ut7.InputSystem;
import act4ut7.bankservices.BankAccount;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 28/05/2018.
 * All Rights Reserved.
 */
public class AddCurrency extends MenuOption {

    public AddCurrency() {
        super("Add currency");
    }

    @Override
    public void run() {
        BankService bs = BankManager.getInstance().getCurrentService();
        if(bs instanceof BankAccount) {
            boolean errors;
            double curr = 0D;
            do {
                curr = InputSystem.readDouble(sc, "Enter the amount to add: ");
                errors = curr < 0;
                if(errors)
                    System.out.println("That's not a valid amount");
            } while(errors);

            ((BankAccount)bs).deposit(curr);
        } else
            System.out.println("You can't add currency to a deposit!");
    }
}
