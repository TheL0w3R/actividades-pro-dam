package act4ut7.options;

import act4ut7.BankManager;
import act4ut7.InputSystem;
import act4ut7.factories.ClientFactory;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class AddClient extends MenuOption {

    public AddClient() {
        super("Register client");
    }

    @Override
    public void run() {
        ClientFactory cf = new ClientFactory();
        cf.setNif(InputSystem.readString(sc, "Enter the client's NIF: "));
        cf.setName(InputSystem.readString(sc, "Enter the client's name: "));
        cf.setTlf(InputSystem.readInt(sc, "Enter the client's phone number: "));
        sc.nextLine();
        BankManager.getInstance().addClient(cf.build());
    }
}
