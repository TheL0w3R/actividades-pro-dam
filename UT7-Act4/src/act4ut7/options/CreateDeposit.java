package act4ut7.options;

import act4ut7.BankManager;
import act4ut7.InputSystem;
import act4ut7.factories.BankDepositFactory;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class CreateDeposit extends MenuOption {

    public CreateDeposit() {
        super("Create Deposit");
    }

    @Override
    public void run() {
        BankDepositFactory bdf = new BankDepositFactory();
        bdf.setName(InputSystem.readString(sc, "Enter the deposit name: "));
        bdf.setDesc(InputSystem.readString(sc, "Enter the deposit description: "));
        bdf.setOffice(InputSystem.readInt(sc, "Enter the deposit office ID: "));
        bdf.setCurrency(InputSystem.readDouble(sc, "Enter the deposit balance: "));
        BankManager.getInstance().addService(bdf.build());
    }
}
