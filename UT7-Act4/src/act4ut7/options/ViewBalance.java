package act4ut7.options;

import act4ut7.BankManager;
import act4ut7.bankservices.BankDeposit;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 28/05/2018.
 * All Rights Reserved.
 */
public class ViewBalance extends MenuOption {

    public ViewBalance() {
        super("View current balance");
    }

    @Override
    public void run() {
        if(BankManager.getInstance().getCurrentService() != null)
            System.out.println("Your balance is: " + BankManager.getInstance().getCurrentService().getCurrency());
        else
            System.out.println("The service has expired or been removed!");
    }
}
