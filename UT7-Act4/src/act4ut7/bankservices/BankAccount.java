package act4ut7.bankservices;

import act4ut7.BankService;
import act4ut7.Client;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class BankAccount extends BankService {

    private String accountNum;

    public BankAccount(int id, String name, String desc, String accountNum, double currency, Client owner) {
        super(id, name, desc);
        this.accountNum = accountNum;
        this.currency = currency;
        this.owner = owner;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(String accountNum) {
        this.accountNum = accountNum;
    }

    public double deposit(double val) {
        this.currency += withTax(val);
        return this.currency;
    }

    public double withdraw(double val) {
        this.currency -= val + (val * TAX);
        return this.currency;
    }
}
