package act4ut7.bankservices;

import act4ut7.BankManager;
import act4ut7.BankService;
import act4ut7.Client;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class BankDeposit extends BankService {

    private String depositNum;
    private double interest;
    private int office;

    public BankDeposit(int id, String name, String desc, String depositNum, double currency, double interest, Client owner, int office) {
        super(id, name, desc);
        this.depositNum = depositNum;
        this.currency = currency;
        this.interest = interest;
        this.owner = owner;
        this.office = office;
    }

    public String getDepositNum() {
        return depositNum;
    }

    public void setDepositNum(String depositNum) {
        this.depositNum = depositNum;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public int getOffice() {
        return office;
    }

    public void setOffice(int office) {
        this.office = office;
    }

    public double withdraw() {
        this.currency = this.currency - (this.currency * TAX);
        BankManager.getInstance().consumeDeposit(this);
        return this.currency;
    }
}
