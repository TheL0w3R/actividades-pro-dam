package act4ut7.factories;

import act4ut7.BankManager;
import act4ut7.Client;
import act4ut7.Util;
import act4ut7.bankservices.BankAccount;

/**
 * Created by TheL0w3R on 27/05/2018.
 * All Rights Reserved.
 */
public class BankAccountFactory {

    private int id;
    private String name;
    private String desc;
    private String accountNum;
    private double currency;
    private Client owner;

    public BankAccountFactory() {
        this.id = Util.consumeID();
        this.accountNum = Util.genAccount();
        this.owner = BankManager.getInstance().getLoggedIn();
        this.currency = 0D;
    }

    public BankAccountFactory setName(String name) {
        this.name = name;
        return this;
    }

    public BankAccountFactory setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public BankAccount build() {
        return new BankAccount(id, name, desc, accountNum, currency, owner);
    }
}
