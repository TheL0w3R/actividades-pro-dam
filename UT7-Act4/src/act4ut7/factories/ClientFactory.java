package act4ut7.factories;

import act4ut7.Client;

public class ClientFactory {

    private String nif;
    private String name = "NULL";
    private int tlf = 000000000;

    public ClientFactory setNif(String nif) {
        this.nif = nif;
        return this;
    }

    public ClientFactory setName(String name) {
        this.name = name;
        return this;
    }

    public ClientFactory setTlf(int tlf) {
        this.tlf = tlf;
        return this;
    }

    public Client build() {
        return new Client(nif, name, tlf);
    }
}