package act4ut7.factories;

import act4ut7.BankManager;
import act4ut7.Client;
import act4ut7.Util;
import act4ut7.bankservices.BankAccount;
import act4ut7.bankservices.BankDeposit;

/**
 * Created by TheL0w3R on 27/05/2018.
 * All Rights Reserved.
 */
public class BankDepositFactory {

    private int id;
    private String name;
    private String desc;
    private double currency;
    private Client owner;
    private String depositNum;
    private double interest;
    private int office;

    public BankDepositFactory() {
        this.id = Util.consumeID();
        this.depositNum = Util.genAccount();
        this.owner = BankManager.getInstance().getLoggedIn();
        this.interest = 0.5;
    }

    public BankDepositFactory setCurrency(double currency) {
        this.currency = currency + (currency * this.interest);
        return this;
    }

    public BankDepositFactory setName(String name) {
        this.name = name;
        return this;
    }

    public BankDepositFactory setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public BankDepositFactory setOffice(int office) {
        this.office = office;
        return this;
    }

    public BankDeposit build() {
        return new BankDeposit(id, name, desc, depositNum, currency, interest, owner, office);
    }
}
