package act4ut7;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public abstract class BankService implements ITax {

    protected int id;
    protected String name;
    protected String desc;
    protected Client owner;
    protected double currency;

    public BankService(int id, String name, String desc) {
        this.id = id;
        this.name = name;
        this.desc = desc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Client getOwner() {
        return owner;
    }

    public void setOwner(Client owner) {
        this.owner = owner;
    }

    public double getCurrency() {
        return currency;
    }

    public void setCurrency(double currency) {
        this.currency = currency;
    }

    protected double withTax(double val) {
        return val - (val * TAX);
    }
}
