import java.util.Scanner;

/**
 * Created by thel0w3r on 2/3/18.
 * All Rights Reserved.
 */
public class Act2UT4 {

    Scanner sc;
    private int[] categories;
    private String[] catNames;
    private int[] houses;

    private Act2UT4() {
        sc = new Scanner(System.in);
        categories = new int[]{0, 0, 0, 0, 0, 0};
        catNames = new String[]{"A", "B", "C", "D", "E"};
    }

    public static void main(String[] args) {
        new Act2UT4().init();
    }

    private void init() {
        initHouses();

        int it = 0;

        while (it < houses.length) {
            try {
                System.out.print("\n" +
                        " 1 - Categoría A\n" +
                        " 2 - Categoría B\n" +
                        " 3 - Categoría C\n" +
                        " 4 - Categoría D\n" +
                        " 5 - Categoría E\n" +
                        " 6 - Sin categoría\n" +
                        "\nSelecciona la categoría para la casa " + (it + 1) + ": ");
                int op = sc.nextInt();
                if(op < 1 || op > 6)
                    throw new Exception();
                categories[op - 1]++;
                houses[it] = (op - 1);
                it++;
            } catch(Exception e) {
                System.err.println("La opción seleccionada no es válida!");
                sc.nextLine();
            }
        }

        System.out.println("Resultados:");

        for(int i = 0; i < houses.length; i++) {
            System.out.println("Categoría para la casa número " + (i + 1) + ": " + ((houses[i] >= 5) ? "Sin categoría" : catNames[houses[i]]));
        }

        System.out.println("");

        for(int k = 0; k < categories.length; k++) {
            if(k == 5)
                System.out.println("Número de casas sin categoría: " + categories[5]);
            else
                System.out.println("Número de casas con categoría " + catNames[k] + ": " + categories[k]);
        }
    }

    private void initHouses() {
        boolean done = false;

        while (!done) {
            try {
                System.out.print("Número de casas a categorizar: ");
                int amount = sc.nextInt();
                houses = new int[amount];
                done = true;
            } catch(Exception e) {
                System.err.println("El dato introducido no es válido!");
                sc.nextLine();
            }
        }
    }

}
