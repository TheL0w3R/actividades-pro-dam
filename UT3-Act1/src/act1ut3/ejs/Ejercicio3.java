package act1ut3.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 21/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio3 extends MenuOption {

    public Ejercicio3() {
        super("Ejercicio 3");
    }

    @Override
    public void run() {
        System.out.print("Escribe un número: ");
        int num = sc.nextInt();
        do {
            System.out.println((char)num);
            num -= 4;
        } while(num > 0);
    }
}
