package act1ut3.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

import java.util.Random;

/**
 * Created by TheL0w3R on 21/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio2 extends MenuOption {

    public Ejercicio2() {
        super("Ejercicio 2");
    }

    @Override
    public void run() {
        int i = 0;
        while(i < 10) {
            int rand = new Random().nextInt(50);
            System.out.print(rand + ", ");
            if(rand % 2 == 0)
                System.out.println("par.");
            else
                System.out.println("impar.");
            i++;
        }
    }
}
