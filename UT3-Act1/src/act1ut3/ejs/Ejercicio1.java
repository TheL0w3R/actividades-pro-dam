package act1ut3.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 21/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio1 extends MenuOption {

    public Ejercicio1() {
        super("Ejercicio 1");
    }

    @Override
    public void run() {
        for(int i = 1; i <= 300; i++) {
            if(i % 5 != 0)
                System.out.println(i);
        }
    }
}
