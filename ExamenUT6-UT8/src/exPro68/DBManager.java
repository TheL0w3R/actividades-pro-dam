package exPro68;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by thel0w3r on 27/5/18.
 * All Rights Reserved.
 */
public class DBManager {

    private static DBManager instance;

    private Connection conn;

    public DBManager() {
        instance = this;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/expro68", "root", "");
        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, "There was an error connecting to the database:\n\n" +
                e.getMessage(),"Connection Error", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
    }

    public static DBManager getInstance() {
        return instance;
    }

    public Animal[] getAllAnimals() {
        ArrayList<Animal> animals = new ArrayList<>();
        try {
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM animals;");
            ResultSet res = ps.executeQuery();
            while(res.next()) {
                animals.add(new Animal(
                        res.getString("chip"),
                        res.getString("name"),
                        res.getString("type"),
                        res.getString("breed"),
                        res.getDouble("weight")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return animals.toArray(new Animal[]{});
    }

    public void addAnimal(Animal a) {
        try {
            PreparedStatement ps = conn.prepareStatement("INSERT INTO animals VALUES('" +
                    a.getChip() + "','" +
                    a.getName() + "','" +
                    a.getType() + "','" +
                    a.getBreed() + "'," +
                    a.getWeight() +
                    ")");

            boolean error = ps.execute();

            if(!error)
                JOptionPane.showMessageDialog(MainWindow.getInstance(),
                        "Animal added successfully!",
                        "Add animal", JOptionPane.INFORMATION_MESSAGE);
            else
                JOptionPane.showMessageDialog(MainWindow.getInstance(),
                        "Couldn't add animal, this chip ID is already added!",
                        "Add animal", JOptionPane.ERROR_MESSAGE);

            MainWindow.getInstance().refreshTable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean addAnimals(Animal[] animals) {
        boolean error = false;
        for(Animal a : animals) {
            try {
                PreparedStatement ps = conn.prepareStatement("INSERT INTO animals VALUES('" +
                        a.getChip() + "','" +
                        a.getName() + "','" +
                        a.getType() + "','" +
                        a.getBreed() + "'," +
                        a.getWeight() +
                        ")");

                error = ps.execute();
                if(error)
                    break;

                MainWindow.getInstance().refreshTable();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return error;
    }

    public void removeByID(String chipID) {
        try {
            PreparedStatement ps = conn.prepareStatement("DELETE FROM animals WHERE(chip LIKE '" + chipID + "')");
            boolean error = ps.execute();
            if(!error)
                JOptionPane.showMessageDialog(MainWindow.getInstance(),
                        "Animal deleted successfully!",
                        "Delete animal", JOptionPane.INFORMATION_MESSAGE);
            else
                JOptionPane.showMessageDialog(MainWindow.getInstance(),
                        "Couldn't delete the animal!",
                        "Delete animal", JOptionPane.ERROR_MESSAGE);

            MainWindow.getInstance().refreshTable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeByType(String type) {
        try {
            PreparedStatement ps = conn.prepareStatement("DELETE FROM animals WHERE(type LIKE '" + type + "')");
            boolean error = ps.execute();
            if(!error)
                JOptionPane.showMessageDialog(MainWindow.getInstance(),
                        "All " + type + "'s deleted successfully!",
                        "Delete animals", JOptionPane.INFORMATION_MESSAGE);
            else
                JOptionPane.showMessageDialog(MainWindow.getInstance(),
                        "Couldn't delete animals!",
                        "Delete animals", JOptionPane.ERROR_MESSAGE);

            MainWindow.getInstance().refreshTable();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String[] getAllTypes() {
        ArrayList<String> types = new ArrayList<>();
        try {
            PreparedStatement ps = conn.prepareStatement("SELECT DISTINCT(type) FROM animals");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
                types.add(rs.getString("type"));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return types.toArray(new String[]{});
    }

}
