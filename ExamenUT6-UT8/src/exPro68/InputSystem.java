package exPro68;

import javax.swing.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class InputSystem {

    public static double getDouble(String msg, String title) {
        double d = -1;
        boolean error;
        do {
            try {
                d = Double.valueOf(JOptionPane.showInputDialog(MainWindow.getInstance(), msg, title, JOptionPane.PLAIN_MESSAGE));
                error = d < 0;
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(MainWindow.getInstance(), "Incorrect or empty data!", "Incorrect Data", JOptionPane.ERROR_MESSAGE);
                error = true;
            }
        } while(error);

        return d;
    }

    public static String getString(String msg, String title) {
        String s = null;
        boolean error;
        do {
            s = JOptionPane.showInputDialog(MainWindow.getInstance(), msg, title, JOptionPane.PLAIN_MESSAGE);
            error = s == null || s.equalsIgnoreCase("");
            if(error)
                JOptionPane.showMessageDialog(MainWindow.getInstance(), "Incorrect or empty data!", "Incorrect Data", JOptionPane.ERROR_MESSAGE);
        } while(error);

        return s;
    }

    public static Animal readAnimal() {
        AnimalFactory af = new AnimalFactory();
        af.setChip(getString("Enter the animal's chip ID", "Add animal"));
        af.setName(getString("Enter the animal's name", "Add animal"));
        af.setType(getString("Enter the animal type", "Add animal"));
        af.setBreed(getString("Enter the animal breed", "Add animal"));
        af.setWeight(getDouble("Enter the animal weight", "Add animal"));
        return af.build();
    }

}
