package exPro68;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by thel0w3r on 28/5/18.
 * All Rights Reserved.
 */
public class FileManager {

    public static void writeAnimalList(String filepath, Animal[] animals) {
        try {
            DataOutputStream dos = new DataOutputStream(new FileOutputStream(filepath));
            for(Animal a : animals) {
                dos.writeUTF(a.getChip());
                dos.writeUTF(a.getName());
                dos.writeUTF(a.getType());
                dos.writeUTF(a.getBreed());
                dos.writeDouble(a.getWeight());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Animal[] readAnimalList(String filepath) {
        ArrayList<Animal> animals = new ArrayList<>();
        try {
            DataInputStream dis = new DataInputStream(new FileInputStream(filepath));
            try {
                while(true) {
                    animals.add(new Animal(
                            dis.readUTF(),
                            dis.readUTF(),
                            dis.readUTF(),
                            dis.readUTF(),
                            dis.readDouble()
                    ));
                }
            } catch(EOFException e) {

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return animals.toArray(new Animal[]{});
    }

}
