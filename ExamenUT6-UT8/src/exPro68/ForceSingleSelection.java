package exPro68;

import javax.swing.*;

/**
 * Created by thel0w3r on 28/5/18.
 * All Rights Reserved.
 */
public class ForceSingleSelection extends DefaultListSelectionModel {

    public ForceSingleSelection () {
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    @Override
    public void clearSelection() {
    }

    @Override
    public void removeSelectionInterval(int index0, int index1) {
    }

}
