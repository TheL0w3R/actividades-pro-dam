package exPro68;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * Created by thel0w3r on 28/5/18.
 * All Rights Reserved.
 */
public class MainWindow extends JFrame {

    private static MainWindow instance;

    private JButton importButton, exportButton, delSelButton, delTypeButton, addButton;
    private JScrollPane tableScroll;
    private JTable contentTable;
    private DefaultTableModel defaultModel;

    public MainWindow() {
        super("Examen Lorenzo [UT6-8]");

        instance = this;

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,"The system look and feel couldn't be applied or found.");
        }
        this.setSize(600, 480);
        this.setResizable(false);
        this.getContentPane().setLayout(new GridBagLayout());
        this.setLocationRelativeTo(null);
        buildLayout();
        setupListeners();
        this.setVisible(true);

        refreshTable();
    }

    public static MainWindow getInstance() {
        return instance;
    }

    private void setupListeners() {
        exportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser();
                int val = fc.showSaveDialog(MainWindow.this);
                if(val == JFileChooser.APPROVE_OPTION) {
                    FileManager.writeAnimalList(fc.getSelectedFile().getPath(), DBManager.getInstance().getAllAnimals());
                    JOptionPane.showMessageDialog(MainWindow.this, "The animal list has been exported to\n" +
                            fc.getSelectedFile().getPath(), "Export animal list", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        importButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser();
                int val = fc.showOpenDialog(MainWindow.this);
                if(val == JFileChooser.APPROVE_OPTION) {
                    DBManager.getInstance().addAnimals(FileManager.readAnimalList(fc.getSelectedFile().getPath()));
                    JOptionPane.showMessageDialog(MainWindow.this, "The animal list has been imported!",
                            "Import animal list", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DBManager.getInstance().addAnimal(InputSystem.readAnimal());
            }
        });
        delSelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(contentTable.getSelectedRow() != -1) {
                    String chipID = (String) defaultModel.getValueAt(contentTable.getSelectedRow(), 0);
                    DBManager.getInstance().removeByID(chipID);
                }
            }
        });
        delTypeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] animalTypes = DBManager.getInstance().getAllTypes();
                JComboBox<String> typesCombo = new JComboBox<>();
                JButton delButton = new JButton("Delete");

                delButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        DBManager.getInstance().removeByType((String)typesCombo.getSelectedItem());
                    }
                });

                for(String type : animalTypes)
                    typesCombo.addItem(type);

                JOptionPane jop = new JOptionPane("Select a type to delete",
                        JOptionPane.PLAIN_MESSAGE,
                        JOptionPane.DEFAULT_OPTION,
                        null,new Object[]{delButton, typesCombo}, null);

                JDialog d = new JDialog();
                d.setTitle("Delete by type");
                d.getContentPane().add(jop);
                d.setLocationRelativeTo(null);
                d.setResizable(false);
                d.pack();
                d.setVisible(true);
            }
        });
    }

    private void buildLayout() {
        importButton = new JButton("Import");
        positionElement(0, 0, 1, 1, 1, 0, importButton);

        exportButton = new JButton("Export");
        positionElement(2, 0, 1, 1, 1, 0, exportButton);

        contentTable = new JTable();
        contentTable.setDragEnabled(false);
        contentTable.setSelectionModel(new ForceSingleSelection());
        defaultModel = new DefaultTableModel(new String[][]{}, new String[]{"Chip", "Name", "Type", "Breed", "Weight"}) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        contentTable.setModel(defaultModel);
        tableScroll = new JScrollPane(contentTable,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        positionElement(0, 1, 3, 1, 1, 1, tableScroll);

        delSelButton = new JButton("Delete selected");
        positionElement(0, 2, 1, 1, 1, 0, delSelButton);

        delTypeButton = new JButton("Delete by type");
        positionElement(1, 2, 1, 1, 1, 0, delTypeButton);

        addButton = new JButton("Add animal");
        positionElement(2, 2, 1, 1, 1, 0, addButton);
    }

    private void positionElement(int x, int y, int xX, int xY, double wX, double wY, JComponent c) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.gridwidth = xX;
        constraints.gridheight = xY;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weighty = wY;
        constraints.weightx = wX;
        constraints.ipadx = 5;
        constraints.ipady = 5;
        constraints.insets = new Insets(10, 10, 10, 10);
        this.getContentPane().add(c, constraints);
        constraints.weighty = 0.0;
        constraints.weightx = 0.0;
    }

    public void refreshTable() {
        Animal[] animals = DBManager.getInstance().getAllAnimals();
        defaultModel.setRowCount(0);
        for(Animal a : animals)
            defaultModel.addRow(a.getRaw());

        defaultModel.fireTableDataChanged();
    }

}
