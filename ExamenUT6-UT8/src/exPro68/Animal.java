package exPro68;

/**
 * Created by thel0w3r on 28/5/18.
 * All Rights Reserved.
 */
public class Animal {
    private String chip, name, type, breed;
    private double weight;

    public Animal(String chip, String name, String type, String breed, double weight) {
        this.chip = chip;
        this.name = name;
        this.type = type;
        this.breed = breed;
        this.weight = weight;
    }

    public String getChip() {
        return chip;
    }

    public void setChip(String chip) {
        this.chip = chip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String[] getRaw() {
        return new String[]{chip, name, type, breed, String.valueOf(weight)};
    }
}
