package exPro68;

/**
 * Created by thel0w3r on 28/5/18.
 * All Rights Reserved.
 */
public class AnimalFactory {

    private String chip, name, type, breed;
    private double weight;

    public void setChip(String chip) {
        this.chip = chip;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Animal build() {
        return new Animal(chip, name, type, breed, weight);
    }
}
