package act1ut7;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class Student extends Person {

    private String cial;
    private String course;

    public Student(String name, String dni, LocalDate birthDate, String cial, String course) {
        super(name, dni, birthDate);
        this.cial = cial;
        this.course = course;
    }

    public String getCial() {
        return cial;
    }

    public void setCial(String cial) {
        this.cial = cial;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }
}
