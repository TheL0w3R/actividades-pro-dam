package act1ut7;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class PersonManager {

    private static PersonManager instance;

    private ArrayList<Person> persons = new ArrayList<>();

    public PersonManager() {
        instance = this;
    }

    public void addTeacher(Teacher t) {
        if(!persons.contains(t))
            persons.add(t);
    }

    public void addStudent(Student s) {
        if(!persons.contains(s))
            persons.add(s);
    }

    public void removeStudent(String name) {
        persons.removeIf(s -> s instanceof Student && s.getName().equalsIgnoreCase(name));
    }

    public void removeTeacher(String name) {
        persons.removeIf(t -> t instanceof Teacher && t.getName().equalsIgnoreCase(name));
    }

    public Person[] getPersons() {
        return persons.toArray(new Person[]{});
    }

    public Teacher[] getTeachers() {
        ArrayList<Teacher> teachers = new ArrayList<>();
        for(Person p : persons) {
            if(p instanceof Teacher)
                teachers.add((Teacher)p);
        }
        return teachers.toArray(new Teacher[]{});
    }

    public Student[] getStudents() {
        ArrayList<Student> teachers = new ArrayList<>();
        for(Person p : persons) {
            if(p instanceof Student)
                teachers.add((Student) p);
        }
        return teachers.toArray(new Student[]{});
    }

    public static PersonManager getInstance() {
        return instance;
    }
}
