package act1ut7.factories;

import act1ut7.Student;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class StudentFactory {

    private String name;
    private String dni;
    private LocalDate birthDate;
    private String cial;
    private String course;

    public StudentFactory setName(String name) {
        this.name = name;
        return this;
    }

    public StudentFactory setDni(String dni) {
        this.dni = dni;
        return this;
    }

    public StudentFactory setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public StudentFactory setCial(String cial) {
        this.cial = cial;
        return this;
    }

    public StudentFactory setCourse(String course) {
        this.course = course;
        return this;
    }

    public Student build() {
        return new Student(name, dni, birthDate, cial, course);
    }
}
