package act1ut7.factories;

import act1ut7.Teacher;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class TeacherFactory {

    private String name;
    private String dni;
    private LocalDate birthDate;
    private int nrp;
    private int spec;
    private String college;

    public TeacherFactory setName(String name) {
        this.name = name;
        return this;
    }

    public TeacherFactory setDni(String dni) {
        this.dni = dni;
        return this;
    }

    public TeacherFactory setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public TeacherFactory setNrp(int nrp) {
        this.nrp = nrp;
        return this;
    }

    public TeacherFactory setSpec(int spec) {
        this.spec = spec;
        return this;
    }

    public TeacherFactory setCollege(String college) {
        this.college = college;
        return this;
    }

    public Teacher build() {
        return new Teacher(name, dni, birthDate, nrp, spec, college);
    }
}
