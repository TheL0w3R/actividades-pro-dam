package act1ut7;

import act1ut7.options.*;
import com.thel0w3r.tlmenuframework.MenuBuilder;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        new PersonManager();
        new MenuBuilder()
                .title("Actividad 1 UT7 [Lorenzo Pinna Rodríguez]")
                .addOption(new AddStudent())
                .addOption(new AddTeacher())
                .addOption(new RemoveStudent())
                .addOption(new RemoveTeacher())
                .addOption(new ShowStudents())
                .addOption(new ShowTeachers())
                .addOption(new ShowAll())
                .build()
                .start();
    }

}
