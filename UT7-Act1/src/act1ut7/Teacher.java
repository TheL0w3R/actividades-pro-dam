package act1ut7;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class Teacher extends Person {

    private int nrp;
    private int spec;
    private String college;

    public Teacher(String name, String dni, LocalDate birthDate, int nrp, int spec, String college) {
        super(name, dni, birthDate);
        this.nrp = nrp;
        this.spec = spec;
        this.college = college;
    }

    public int getNrp() {
        return nrp;
    }

    public void setNrp(int nrp) {
        this.nrp = nrp;
    }

    public int getSpec() {
        return spec;
    }

    public void setSpec(int spec) {
        this.spec = spec;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }
}
