package act1ut7.options;

import act1ut7.InputSystem;
import act1ut7.PersonManager;
import act1ut7.factories.TeacherFactory;
import com.thel0w3r.tlmenuframework.MenuOption;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class AddTeacher extends MenuOption {

    public AddTeacher() {
        super("Add a new Teacher");
    }

    @Override
    public void run() {
        TeacherFactory tf = new TeacherFactory();
        tf.setDni(InputSystem.readString(sc, "Enter the teacher DNI: "));
        tf.setName(InputSystem.readString(sc, "Enter the teacher name: "));
        tf.setBirthDate(LocalDate.of(
                InputSystem.readInt(sc, "Enter the teacher birth year: "),
                InputSystem.readInt(sc, "Enter the teacher birth month: "),
                InputSystem.readInt(sc, "Enter the teacher birth day: ")
        ));
        sc.nextLine();
        tf.setNrp(InputSystem.readInt(sc, "Enter the teacher NRP: "));
        tf.setSpec(InputSystem.readInt(sc, "Enter the teacher speciality ID: "));
        sc.nextLine();
        tf.setCollege(InputSystem.readString(sc, "Enter the teacher college: "));

        PersonManager.getInstance().addTeacher(tf.build());

        System.out.println("\nTeacher added successfully!");
    }

}
