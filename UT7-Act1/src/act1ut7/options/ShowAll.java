package act1ut7.options;

import act1ut7.Person;
import act1ut7.PersonManager;
import act1ut7.Student;
import act1ut7.Teacher;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class ShowAll extends MenuOption {

    public ShowAll() {
        super("Show all persons");
    }

    @Override
    public void run() {
        PersonManager pm = PersonManager.getInstance();
        StringBuilder sb = new StringBuilder("Registered persons:\n\n");
        for (Person p : pm.getPersons()) {
            sb.append(" ").append(p.getName()).append(" (").append(p instanceof Teacher ? "Teacher" : "Student").append(")\n");
            sb.append("  - DNI: ").append(p.getDni()).append("\n");
            sb.append("  - Birth Date: ").append(p.getBirthDate()).append("\n");
            sb.append("  - Age: ").append(p.age()).append("\n");
            if(p instanceof Teacher) {
                Teacher t = (Teacher)p;
                sb.append("  - NRP: ").append(t.getNrp()).append("\n");
                sb.append("  - College: ").append(t.getCollege()).append("\n");
                sb.append("  - Speciality: ").append(t.getSpec()).append("\n");
            } else if(p instanceof Student) {
                Student s = (Student)p;
                sb.append("  - CIAL: ").append(s.getCial()).append("\n");
                sb.append("  - Course: ").append(s.getCourse()).append("\n");
            }
            sb.append("\n");
        }

        System.out.print(sb);
    }
}
