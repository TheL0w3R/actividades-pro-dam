package act1ut7.options;

import act1ut7.InputSystem;
import act1ut7.Person;
import act1ut7.PersonManager;
import act1ut7.Student;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class RemoveStudent extends MenuOption {

    public RemoveStudent() {
        super("Remove a Student");
    }

    @Override
    public void run() {
        PersonManager pm = PersonManager.getInstance();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < pm.getStudents().length; i++) {
            sb.append(" ").append(i+1).append(" - ").append(pm.getStudents()[i].getName()).append("\n");
        }
        sb.append(" ").append(pm.getStudents().length + 1).append(" - Back");
        System.out.println(sb);
        int op = InputSystem.readInt(sc, "\nSelect a student to remove: ");
        boolean removed = false;
        if(op != pm.getStudents().length + 1) {
            while(op < 1 || op > pm.getStudents().length)  {
                System.out.println("The selected student is not valid!");
                op = InputSystem.readInt(sc, "Select a student to remove: ");
            }
            pm.removeStudent(pm.getStudents()[op - 1].getName());
            System.out.println("\nStudent removed successfully!");
        }
    }
}
