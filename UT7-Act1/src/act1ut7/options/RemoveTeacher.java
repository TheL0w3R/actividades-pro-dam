package act1ut7.options;

import act1ut7.InputSystem;
import act1ut7.PersonManager;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class RemoveTeacher extends MenuOption {

    public RemoveTeacher() {
        super("Remove a Teacher");
    }

    @Override
    public void run() {
        PersonManager pm = PersonManager.getInstance();
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < pm.getTeachers().length; i++) {
            sb.append(" ").append(i+1).append(" - ").append(pm.getTeachers()[i].getName()).append("\n");
        }
        sb.append(" ").append(pm.getTeachers().length + 1).append(" - Back");
        System.out.println(sb);
        int op = InputSystem.readInt(sc, "\nSelect a teacher to remove: ");
        boolean removed = false;
        if(op != pm.getTeachers().length + 1) {
            while(op < 1 || op > pm.getTeachers().length)  {
                System.out.println("The selected teacher is not valid!");
                op = InputSystem.readInt(sc, "Select a teacher to remove: ");
            }
            pm.removeTeacher(pm.getTeachers()[op - 1].getName());
            System.out.println("\nTeacher removed successfully!");
        }
    }
}
