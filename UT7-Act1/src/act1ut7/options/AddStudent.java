package act1ut7.options;

import act1ut7.InputSystem;
import act1ut7.PersonManager;
import act1ut7.factories.StudentFactory;
import com.thel0w3r.tlmenuframework.MenuOption;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class AddStudent extends MenuOption {

    public AddStudent() {
        super("Add a new Student");
    }

    @Override
    public void run() {
        StudentFactory sf = new StudentFactory();
        sf.setDni(InputSystem.readString(sc, "Enter the student DNI: "));
        sf.setName(InputSystem.readString(sc, "Enter the student name: "));
        sf.setBirthDate(LocalDate.of(
                InputSystem.readInt(sc, "Enter the student birth year: "),
                InputSystem.readInt(sc, "Enter the student birth month: "),
                InputSystem.readInt(sc, "Enter the student birth day: ")
        ));
        sc.nextLine();
        sf.setCial(InputSystem.readString(sc, "Enter the student CIAL: "));
        sf.setCourse(InputSystem.readString(sc, "Enter the student course: "));

        PersonManager.getInstance().addStudent(sf.build());

        System.out.println("\nStudent added successfully!");
    }
}
