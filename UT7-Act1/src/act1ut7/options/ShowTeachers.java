package act1ut7.options;

import act1ut7.PersonManager;
import act1ut7.Student;
import act1ut7.Teacher;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class ShowTeachers extends MenuOption {

    public ShowTeachers() {
        super("Show Teachers");
    }

    @Override
    public void run() {
        PersonManager pm = PersonManager.getInstance();
        StringBuilder sb = new StringBuilder("Registered teachers:\n\n");
        for (Teacher t : pm.getTeachers()) {
            sb.append(" ").append(t.getName()).append("\n");
            sb.append("  - DNI: ").append(t.getDni()).append("\n");
            sb.append("  - Birth Date: ").append(t.getBirthDate()).append("\n");
            sb.append("  - Age: ").append(t.age()).append("\n");
            sb.append("  - NRP: ").append(t.getNrp()).append("\n");
            sb.append("  - College: ").append(t.getCollege()).append("\n");
            sb.append("  - Speciality: ").append(t.getSpec()).append("\n");
        }

        System.out.print(sb);
    }
}
