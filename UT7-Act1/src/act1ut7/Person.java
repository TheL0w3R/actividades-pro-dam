package act1ut7;

import java.time.LocalDate;
import java.time.Period;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class Person {

    private String name;
    private String dni;
    private LocalDate birthDate;

    public Person(String name, String dni, LocalDate birthDate) {
        this.name = name;
        this.dni = dni;
        this.birthDate = birthDate;
    }

    public int age() {
        return Period.between(birthDate, LocalDate.now()).getYears();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }
}
