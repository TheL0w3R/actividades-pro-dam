package act1ut7;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class InputSystem {

    public static String readString(Scanner sc, String msg) {
        String s = "";
        boolean error = false;
        do {
            try {
                System.out.print(msg);
                s = sc.nextLine();
                error = false;
            } catch (InputMismatchException e) {
                System.out.println("Data type is incorrect.");
                sc.nextLine();
                error = true;
            }
        } while(error);

        return s;
    }

    public static int readInt(Scanner sc, String msg) {
        int i = 0;
        boolean error = false;
        do {
            try {
                System.out.print(msg);
                i = sc.nextInt();
                error = false;
            } catch (InputMismatchException e) {
                System.out.println("Data type is incorrect.");
                sc.nextLine();
                error = true;
            }
        } while(error);

        return i;
    }

}
