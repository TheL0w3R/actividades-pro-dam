package clase;

import clase.ejs.*;
import com.thel0w3r.tlmenuframework.Locale;
import com.thel0w3r.tlmenuframework.Menu;
import com.thel0w3r.tlmenuframework.MenuBuilder;

/**
 * Created by TheL0w3R on 08/11/2017.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        Menu m = new MenuBuilder()
                .title("Actividades de clase [Lorenzo Pinna Rodríguez]")
                .setLocale(Locale.ES)
                .addOption(new Cesar())
                .addOption(new WhileDiv3())
                .addOption(new Substring())
                .addOption(new Cesar2())
                .addOption(new ProCypher())
                .addOption(new NotasAlumnos())
                .addOption(new Vectors())
                .addOption(new StudentsVectors())
                .addOption(new HelloWorldSwing())
                .build();
        m.start();
    }

}
