package clase;

import javax.swing.*;
import java.awt.*;

/**
 * Created by thel0w3r on 7/6/18.
 * All Rights Reserved.
 */
public class Superplantilla extends JFrame {

    public Superplantilla() {
        super("Super plantilla xd lol");

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,"The system look and feel couldn't be applied or found.");
        }
        this.setSize(600, 480);
        this.setResizable(false);
        this.getContentPane().setLayout(new GridBagLayout());
        this.setLocationRelativeTo(null);
        JTextArea texto = new JTextArea();
        JButton boton1 = new JButton("Boton1");
        JButton boton2 = new JButton("Boton2");
        positionElement(0, 0, 3, 1, 1, 1, texto);
        positionElement(0, 1, 1, 1, 1, 0, boton1);
        positionElement(1, 1, 1, 1, 1, 0, new JPanel());
        positionElement(2, 1, 1, 1, 1, 0, boton2);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        new Superplantilla();
    }

    private void positionElement(int x, int y, int xX, int xY, double wX, double wY, JComponent c) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.gridwidth = xX;
        constraints.gridheight = xY;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weighty = wY;
        constraints.weightx = wX;
        constraints.ipadx = 5;
        constraints.ipady = 5;
        constraints.insets = new Insets(10, 10, 10, 10);
        this.getContentPane().add(c, constraints);
        constraints.weighty = 0.0;
        constraints.weightx = 0.0;
    }
}
