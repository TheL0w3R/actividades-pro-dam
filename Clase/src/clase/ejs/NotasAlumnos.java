package clase.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

import java.io.IOException;
import java.util.InputMismatchException;

/**
 * Created by TheL0w3R on 20/11/2017.
 * All Rights Reserved.
 */
public class NotasAlumnos extends MenuOption {

    public NotasAlumnos() {
        super("Notas alumnos");
    }

    @Override
    public void run() {
        int ap = 0;
        int su = 0;
        int ne = 0;
        System.out.print("Número de alumnos a evaluar: ");
        int amount = sc.nextInt();
        System.out.println(" A - Aprobado.\n S - Suspendido.\n N - No evaluado.");
        int inc = 0;
        while(inc < amount) {
            System.out.print("Introduce el valor para el " + (inc+1) + "º alumno: ");
            try {
                char val = Character.toUpperCase(sc.next().charAt(0));
                switch(val) {
                    case 'A':
                        ap++;
                        inc++;
                        break;
                    case 'S':
                        su++;
                        inc++;
                        break;
                    case 'N':
                        ne++;
                        inc++;
                        break;
                    default:
                        System.out.println("No es una opción válida.lol");
                        break;
                }
            } catch (InputMismatchException e) {
                System.out.println("No es una opción válida.");
            }
        }
        double tot = ap + su + ne;
        System.out.println(ap + " alumnos aprobados (" + getPercentage(ap, tot) + "%).");
        System.out.println(su + " alumnos suspendidos (" + getPercentage(su, tot) + "%).");
        System.out.println(ne + " alumnos no evaluados (" + getPercentage(ne, tot) + "%).");
    }

    private double getPercentage(double n, double tot) {
        return ((n/tot)*100);
    }
}
