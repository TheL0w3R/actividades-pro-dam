package clase.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 10/11/2017.
 * All Rights Reserved.
 */
public class Substring extends MenuOption {

    public Substring() {
        super("Substrings 1");
    }

    @Override
    public void run() {
        System.out.print("Escribe tu nombre con apellidos: ");
        String fullName = sc.nextLine();
        String name = fullName.substring(0, fullName.indexOf(" "));
        String str = fullName.substring(fullName.indexOf(" ") + 1) + ", " + name;
        System.out.println("Nombre: " + str.substring(str.indexOf(",") + 2));
        System.out.println("Apellido 1: " + str.substring(0, str.indexOf(" ")));
        System.out.println("Apellido 2: " + str.substring(str.indexOf(" ") + 1, str.indexOf(",")));
    }
}
