package clase.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 27/11/2017.
 * All Rights Reserved.
 */
public class Vectors extends MenuOption {

    private final int NUM_EVAL = 3;

    public Vectors() {
        super("Vectores");
    }

    @Override
    public void run() {
        for(int i = 1; i <= 10; i++) {
            double[] marks = new double[NUM_EVAL];
            for(int k = 0; k < marks.length; k++) {
                System.out.print(i + "º alumno, EV" + (k + 1) + ": ");
                marks[k] = sc.nextDouble();
            }
            System.out.println("La media para el " + i + "º alumno es: " + getMed(marks));
        }
    }

    private double getMed(double[] marks) {
        double sum = 0;
        for(double mark : marks) {
            sum += mark;
        }
        return (double)Math.round((sum/marks.length) * 1000) / 1000;
    }
}
