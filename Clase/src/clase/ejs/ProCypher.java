package clase.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

import java.util.ArrayList;

/**
 * Created by TheL0w3R on 17/11/2017.
 * All Rights Reserved.
 */
public class ProCypher extends MenuOption {

    private final String REGEX     = "abcdefghijklmnñopqrstuvwxyz ";
    private final String REPLACER  = "64hgsk3vbnñpawqx.?fç1_d~+z¡[";

    public ProCypher() {
        super("Cifrado avanzado");
    }

    @Override
    public void run() {
        while (true) {
            System.out.println(" 1 - Cifrar texto");
            System.out.println(" 2 - Descifrar texto");
            System.out.println(" 3 - Salir\n");
            System.out.print("Selecciona una opción: ");
            int o = sc.nextInt();
            if(o == 3)
                break;

            sc.nextLine();
            System.out.print("Escribe el texto: ");
            String text = sc.nextLine();
            if(o == 1) {
                System.out.println(cypher(text));
            } else if (o == 2)
                System.out.println(deCypher(text));
            else
                System.out.println("Opción no válida");
        }
    }

    private String cypher(String s) {
        ArrayList<Integer> bl = new ArrayList<>();
        StringBuilder sb = new StringBuilder(s);
        for(int i = 0; i < REGEX.length(); i++) {
            char orig = REGEX.charAt(i);
            char rep = REPLACER.charAt(i);
            for(int k = 0; k < s.length(); k++) {
                if(s.charAt(k) == orig && !bl.contains(k)) {
                    sb.setCharAt(k, rep);
                    bl.add(k);
                }
            }
        }
        return sb.toString();
    }

    private String deCypher(String s) {
        ArrayList<Integer> bl = new ArrayList<>();

        StringBuilder sb = new StringBuilder(s);

        for(int i = 0; i < REGEX.length(); i++) {
            char orig = REPLACER.charAt(i);
            char rep = REGEX.charAt(i);
            for(int k = 0; k < s.length(); k++) {
                if(s.charAt(k) == orig && !bl.contains(k)) {
                    sb.setCharAt(k, rep);
                    bl.add(k);
                }
            }
        }
        return sb.toString();
    }
}
