package clase.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by thel0w3r on 19/2/18.
 * All Rights Reserved.
 */
public class HelloWorldSwing extends MenuOption{

    public HelloWorldSwing() {
        super("Hola mundo con Swing!");
    }

    @Override
    public void run() {
        JFrame mainWindow = new JFrame("Hola mundo Swing!");
        mainWindow.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        JPanel mainpanel = new JPanel(new FlowLayout());
        JLabel label = new JLabel("Hola mundo Swing");
        JButton btn = new JButton("Click me!");
        mainpanel.add(label);
        mainpanel.add(btn);
        mainWindow.getContentPane().add(mainpanel);
        mainWindow.pack();
        mainWindow.setLocationRelativeTo(null);
        mainWindow.setVisible(true);

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(mainWindow, "Hi");
            }
        });
    }
}
