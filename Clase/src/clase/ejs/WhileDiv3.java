package clase.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 08/11/2017.
 * All Rights Reserved.
 */
public class WhileDiv3 extends MenuOption {

    public WhileDiv3() {
        super("While decreciente que muestra divisibles por 3");
    }

    @Override
    public void run() {
        int num = 20;
        while(num >= 0) {
            if(num % 3 == 0)
                System.out.println(num);
            num--;
        }
    }
}
