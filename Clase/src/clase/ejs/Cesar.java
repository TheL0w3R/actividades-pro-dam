package clase.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

import java.util.InputMismatchException;

/**
 * Created by TheL0w3R on 08/11/2017.
 * All Rights Reserved.
 */
public class Cesar extends MenuOption {

    private final int LIMIT = 15;

    public Cesar() {
        super("Cifrado César");
    }

    @Override
    public void run() {
        while(true) {
            try {
                System.out.print("  1 - Codificar.\n  2 - Decodificar.\n  3 - Salir.\n\nEscribe una opción: ");
                int option = sc.nextInt();

                if(option == 3)
                    break;

                System.out.print("Indica el desfase de los caracteres: ");
                int offset = sc.nextInt();
                sc.nextLine();

                if(offset <= LIMIT) {
                    System.out.print("Introduce el texto: ");
                    String text = sc.nextLine();

                    if(option == 1) {
                        System.out.println("Tu texto codificado: " + code(text, offset));
                    } else if(option == 2) {
                        System.out.println("Tu texto decodificado: " + decode(text, offset));
                    }
                } else
                    System.out.println("\nEl límite de caracteres de desfase es " + LIMIT + "\n");
            } catch (InputMismatchException e) {
                System.out.println("La entrada no es válida.");
                sc.nextLine();
            }
        }
    }

    private String code(String str, int offset) {
        StringBuilder sb = new StringBuilder();
        for(char c : str.toCharArray()) {
            sb.append((char)(((int) c) + offset));
        }

        return sb.toString();
    }

    public String decode(String str, int offset) {
        StringBuilder sb = new StringBuilder();
        for(char c : str.toCharArray()) {
            sb.append((char)(((int) c) - offset));
        }

        return sb.toString();
    }
}
