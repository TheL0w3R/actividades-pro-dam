package clase.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 14/11/2017.
 * All Rights Reserved.
 */
public class Cesar2 extends MenuOption {

    public Cesar2() {
        super("Cifrado Cesar 2");
    }

    @Override
    public void run() {
        System.out.print("Escribe un texto: ");
        String text = sc.nextLine();
        System.out.print("Indica el desface de los caracteres: ");
        int offset = sc.nextInt();
        char c = text.charAt(0);
        System.out.print(text.replace(c, (char)(((int)c) + offset)));
    }
}
