package clase.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 27/11/2017.
 * All Rights Reserved.
 */
public class StudentsVectors extends MenuOption {

    public StudentsVectors() {
        super("Práctica con vectores (alumnos).");
    }

    @Override
    public void run() {
        System.out.print("Número de alumnos: ");
        int l = sc.nextInt();
        String[] students = new String[l];
        sc.nextLine();
        for(int i = 0; i < students.length; i++) {
            System.out.print("Introduce el nombre del " + (i + 1) + "º alumno: ");
            students[i] = sc.nextLine();
        }
        System.out.println("El alumno con el nombre más corto es: " + getShorter(students));
        System.out.println("El alumno con el nombre más largo es: " + getLarger(students));
    }

    private String getLarger(String[] s) {
        String tmp = s[0];
        for(String st : s) {
            if(st.length() > tmp.length())
                tmp = st;
        }
        return tmp;
    }

    private String getShorter(String[] s) {
        String tmp = s[0];
        for(String st : s) {
            if(st.length() < tmp.length())
                tmp = st;
        }
        return tmp;
    }
}
