package act2ut5;

import java.util.ArrayList;

/**
 * Created by thel0w3r on 7/3/18.
 * All Rights Reserved.
 */
public class Account {
    private String DNI;
    private String accountNum;
    private double currency;
    private StringBuilder movements;

    public Account(String DNI, String accountNum) {
        this.DNI = DNI;
        this.accountNum = accountNum;
        this.currency = 0;
        movements = new StringBuilder();
    }

    public String getDNI() {
        return DNI;
    }

    public String getAccountNum() {
        return accountNum;
    }

    public double getCurrency() {
        return currency;
    }

    public void logMovement(String movement) {
        this.movements.append(" ·").append(movement).append("\n");
    }

    public String getMovements() {
        return movements.toString();
    }

    public void addCurrency(double curr) {
        this.currency += curr;
    }

    public void remCurrency(double curr) {
        this.currency -= curr;
    }
}
