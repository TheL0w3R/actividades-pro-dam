package act2ut5;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by thel0w3r on 7/3/18.
 * All Rights Reserved.
 */
public class BankSystem {

    private Set<Account> accounts;
    private Account active;

    public BankSystem() {
        accounts = new HashSet<>();
        active = null;
    }

    public String registerAccount(String DNI) {
        String accNum = Util.genAccount();
        if(!accounts.add(new Account(DNI, accNum)))
            return null;
        else
            return accNum;
    }

    public Account[] getAccounts() {
        return accounts.toArray(new Account[accounts.size()]);
    }

    public Account[] getAccounts(String DNI) {
        ArrayList<Account> tmp = new ArrayList<>();
        for(Account a : accounts) {
            if(a.getDNI().equalsIgnoreCase(DNI))
                tmp.add(a);
        }
        return tmp.toArray(new Account[tmp.size()]);
    }

    public void selectAccount(Account a) {
        this.active = a;
    }

    public Account getActive() {
        return active;
    }

    public void addCurrency(double curr) {
        active.logMovement("Ingreso: " + curr + "€");
        active.addCurrency(curr);
        System.out.println("Operación completada con éxito");
    }

    public void removeCurr(double curr) {
        if(active.getCurrency() >= curr) {
            active.logMovement("Retiro: " + curr + "€");
            active.remCurrency(curr);
            System.out.println("Operación completada con éxito");
        } else
            System.out.println("No tiene suficiente saldo en su cuenta.");
    }

    public void deleteAccount(Account a) {
        this.accounts.remove(a);
    }
}
