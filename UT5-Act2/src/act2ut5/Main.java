package act2ut5;

import com.thel0w3r.tlmenuframework.Locale;
import com.thel0w3r.tlmenuframework.Menu;
import com.thel0w3r.tlmenuframework.MenuBuilder;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 7/3/18.
 * All Rights Reserved.
 */
public class Main {

    private BankSystem bs;
    private Menu mainMenu;

    public Main() {
        bs = new BankSystem();
        mainMenu = new MenuBuilder()
                .title("Actividad cuentas bancarias [Lorenzo Pinna Rodríguez]")
                .setLocale(Locale.ES)
                .addOption(new MenuOption("Ver cuentas") {
                    @Override
                    public void run() {
                        Account[] acc = bs.getAccounts();
                        System.out.println(acc.length < 1 ? "No hay cuentas registradas." : "Cuentas registradas:\n");

                        for(Account a : bs.getAccounts())
                            System.out.println(" - [" + a.getDNI() + "]: " + a.getAccountNum());
                    }
                })
                .addOption(new MenuOption("Crear cuenta") {
                    @Override
                    public void run() {
                        boolean correct = false;
                        String DNI;

                        do {
                            System.out.print("Introduce tu DNI: ");
                            DNI = sc.nextLine();
                            if(!Util.validateDNI(DNI))
                                System.out.println("El DNI introducido no es válido.");
                            else
                                correct = true;
                        } while(!correct);

                        String accNum = bs.registerAccount(DNI);

                        if(accNum != null)
                            System.out.println("Cuenta " + accNum + " creada con éxito!");
                        else
                            System.out.println("Ya existe esta cuenta");
                    }
                })
                .addOption(new MenuOption("Eliminar cuenta") {
                    @Override
                    public void run() {
                        Account[] acc = bs.getAccounts();
                        if(acc.length < 1)
                            System.out.println("No hay cuentas registradas.");
                        else {
                            System.out.println("Cuentas disponibles:\n");
                            for (int i = 0; i < acc.length; i++)
                                System.out.println(" " + (i + 1) + " - [" + acc[i].getDNI() + "]: " + acc[i].getAccountNum());

                            int op = Util.readInt("\nSelecciona una cuenta: ", 1, acc.length);

                            bs.deleteAccount(acc[(op - 1)]);

                            System.out.println("Cuenta eliminada con éxito.");
                        }
                    }
                })
                .addOption(new MenuOption("Operaciones") {
                    @Override
                    public void run() {
                        Account[] acc = bs.getAccounts();
                        if(acc.length < 1)
                            System.out.println("No hay cuentas registradas.");
                        else {
                            System.out.println("Cuentas disponibles:\n");
                            for (int i = 0; i < acc.length; i++)
                                System.out.println(" " + (i + 1) + " - [" + acc[i].getDNI() + "]: " + acc[i].getAccountNum());

                            int op = Util.readInt("\nSelecciona una cuenta para operar sobre ella: ", 1, acc.length);

                            bs.selectAccount(acc[(op - 1)]);

                            StringBuilder sb = new StringBuilder();
                            sb.append("\n").append("[Cuenta: ").append(bs.getActive().getAccountNum()).append("]\n")
                                    .append(" 1 - Información de la cuenta\n")
                                    .append(" 2 - Consultar movimientos\n")
                                    .append(" 3 - Retirar efectivo\n")
                                    .append(" 4 - Ingresos\n")
                                    .append(" 5 - Cerrar sesión");

                            boolean session = true;
                            while (session) {
                                System.out.println(sb);
                                int o = Util.readInt("\nSelecciona una operación: ", 1, 5);
                                switch (o) {
                                    case 1:
                                        System.out.println("\nPropietario: " + bs.getActive().getDNI());
                                        System.out.println("Saldo actual: " + bs.getActive().getCurrency() + "€");
                                        break;
                                    case 2:
                                        System.out.println("\nSaldo actual: " + bs.getActive().getCurrency() + "€");
                                        if(bs.getActive().getMovements() == null || bs.getActive().getMovements().equals(""))
                                            System.out.println("No hay movimientos recientes.");
                                        else
                                            System.out.println(bs.getActive().getMovements());
                                        break;
                                    case 3:
                                        bs.removeCurr(Util.readDouble("Introduzca la cantidad a retirar: ", 1D, Double.MAX_VALUE));
                                        break;
                                    case 4:
                                        bs.addCurrency(Util.readDouble("Introduzca la cantidad a ingresar: ", 1D, Double.MAX_VALUE));
                                        break;
                                    case 5:
                                        session = false;
                                        break;
                                }
                            }
                        }

                    }
                })
                .build();
    }

    public static void main(String[] args) {
        new Main().init();
    }


    public void init() {
        mainMenu.start();

    }

}
