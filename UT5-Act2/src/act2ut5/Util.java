package act2ut5;

import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by thel0w3r on 7/3/18.
 * All Rights Reserved.
 */
public class Util {

    private static final String upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    private static final String digits = "0123456789";

    private static String gen(int length, String sym) {
        if (length < 1) throw new IllegalArgumentException();
        Random rand = new Random();
        char[] buffer = new char[length];
        for(int i = 0; i < length; i++)
            buffer[i] = sym.charAt(rand.nextInt(length));

        return new String(buffer);
    }

    public static String genAccount() {
        StringBuilder sb = new StringBuilder();
        sb.append(gen(2, upper))
                .append(gen(2, digits))
                .append(" ")
                .append(gen(4, digits))
                .append(" ")
                .append(gen(4, digits))
                .append(" ")
                .append(gen(4, digits))
                .append(" ")
                .append(gen(4, digits))
                .append(" ")
                .append(gen(4, digits));

        return sb.toString();
    }

    public static boolean validateDNI(String dni) {
        boolean correct = false;
        Pattern pattern = Pattern.compile("(\\d{1,8})([TRWAGMYFPDXBNJZSQVHLCKEtrwagmyfpdxbnjzsqvhlcke])");
        Matcher matcher = pattern.matcher(dni);

        if (matcher.matches()) {
            String letter = matcher.group(2);
            String letters = "TRWGAMYFPDXBNJZSQVHLCKE";
            int index = Integer.parseInt(matcher.group(1));
            index = index % 23;
            String reference = letters.substring(index, index + 1);
            correct = reference.equalsIgnoreCase(letter);
        }

        return correct;
    }

    public static int readInt(String msg, int min, int max) {
        Scanner sc = new Scanner(System.in);
        int tmp = 0;
        do {
            System.out.print(msg);
            try {
                tmp = sc.nextInt();
                if(tmp < min || tmp > max)
                    throw new InputMismatchException();
                break;
            } catch (InputMismatchException e) {
                System.out.println("No es una opción válida.");
                sc.nextLine();
            }
        } while(true);

        return tmp;
    }

    public static double readDouble(String msg, double min, double max) {
        Scanner sc = new Scanner(System.in);
        double tmp = 0;
        do {
            System.out.print(msg);
            try {
                tmp = sc.nextDouble();
                if(tmp < min || tmp > max)
                    throw new InputMismatchException();
                break;
            } catch (InputMismatchException e) {
                System.out.println("No es un dato válido.");
                sc.nextLine();
            }
        } while(true);

        return tmp;
    }
}
