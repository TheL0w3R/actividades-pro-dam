package act3ut2.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 08/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio1 extends MenuOption {

    public Ejercicio1() {
        super("Ejercicio 1");
    }

    @Override
    public void run() {
        int num = 0;
        do {
            System.out.print("Escribe un número: ");
            num = sc.nextInt();
            if(num < 0)
                System.out.println("No puedes introducir cifras negativas.");
        } while (num < 0);
        int resul = num;
        num--;
        while(num >= 1) {
            resul *= num;
            num--;
        }

        System.out.println("El factorial de " + num + " es: " + resul);
    }
}
