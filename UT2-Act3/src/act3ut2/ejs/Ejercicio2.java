package act3ut2.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 21/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio2 extends MenuOption {

    public Ejercicio2() {
        super("Ejercicio 2");
    }

    @Override
    public void run() {
        System.out.print("Escribe un número: ");
        int num = sc.nextInt();
        if(num < 0)
            System.out.println("No puedes introducir cifras negativas.");
        System.out.println("El factorial de " + num + " es: " + getFactorial(num));
    }

    private int getFactorial(int num) {
        if(num >= 1)
            return getFactorial(num - 1) * num;
        else
            return 1;
    }
}
