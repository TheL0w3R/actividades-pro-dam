package act3ut2;

import act3ut2.ejs.Ejercicio1;
import act3ut2.ejs.Ejercicio2;
import com.thel0w3r.tlmenuframework.Locale;
import com.thel0w3r.tlmenuframework.Menu;
import com.thel0w3r.tlmenuframework.MenuBuilder;

/**
 * Created by TheL0w3R on 08/11/2017.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        Menu m = new MenuBuilder()
                .title("Actividad 3 UT2 [Lorenzo Pinna Rodríguez]")
                .setLocale(Locale.ES)
                .addOption(new Ejercicio1())
                .addOption(new Ejercicio2())
                .build();
        m.start();
    }

}
