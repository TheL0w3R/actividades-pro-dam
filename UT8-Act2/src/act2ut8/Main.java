package act2ut8;

import java.sql.*;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {

        new Main().init();

    }

    private void init() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ACT2UT8", "root", "");

            System.out.println("Adding the thing 'Testinggg':");
            PreparedStatement insertExample = conn.prepareStatement("INSERT INTO THING(name) VALUES('Testinggg')");
            boolean hasError = insertExample.execute();
            if(!hasError)
                System.out.println(" - Query executed successfully!");
            else
                System.err.println(" - There was an error executing the query!");

            showAll(conn);

            System.out.println("Deleting everything from the table:");
            PreparedStatement deleteExample = conn.prepareStatement("DELETE FROM THING");
            hasError = deleteExample.execute();
            if(!hasError)
                System.out.println(" - Query executed successfully!");
            else
                System.err.println(" - There was an error executing the query!");

            showAll(conn);

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    private void showAll(Connection conn) throws SQLException {
        System.out.println("\nShowing the content of the table:");

        PreparedStatement selectStatement = conn.prepareStatement("SELECT * FROM THING");
        ResultSet rs = selectStatement.executeQuery();
        rs.last();
        String[][] resultTable = new String[rs.getRow() + 1][2];
        rs.beforeFirst();
        resultTable[0][0] = "ID";
        resultTable[0][1] = "NAME";
        int rowIndex = 1;
        while(rs.next()) {
            resultTable[rowIndex][0] = rs.getString("id");
            resultTable[rowIndex][1] = rs.getString("name");
            rowIndex++;
        }

        Util.printTable(resultTable, "Table: THING");
    }

}
