package act1ut6;

import act1ut6.Actions.OperationAction;
import act1ut6.Actions.ValueAction;
import act1ut6.Utilities.Operation;
import act1ut6.Utilities.OperationButton;
import act1ut6.Utilities.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by thel0w3r on 23/3/18.
 * All Rights Reserved.
 */
public class MainWindow extends JFrame {

    private Calculator calculator;

    private JTextField resultArea;
    private JButton cancelBtn;
    private JButton pnBtn;
    private JButton divideBtn;
    private JButton[] numBtns;
    private JButton timesBtn;
    private JButton minusBtn;
    private JButton plusBtn;
    private JButton equalsBtn;
    private JButton commaBtn;

    public static void main(String[] args) {
        new MainWindow();
    }

    private MainWindow() {
        super("Calculadora");
        calculator = new Calculator();

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,"The system look and feel couldn't be applied or found.");
        }
        this.setSize(300, 400);
        this.setResizable(false);
        this.getContentPane().setLayout(new GridBagLayout());
        this.setLocationRelativeTo(null);
        buildLayout();
        setupKeyListener();
        this.setVisible(true);
    }

    private void setupKeyListener() {
        Toolkit.getDefaultToolkit().addAWTEventListener( new AWTEventListener()
        {
            public void eventDispatched(AWTEvent e)
            {
                if(e instanceof KeyEvent && e.getID() == KeyEvent.KEY_PRESSED) {
                    char key = ((KeyEvent) e).getKeyChar();
                    if(Character.isDigit(key) || key == ',' || key == '.') {
                        calculator.addToBuffer(String.valueOf(key).replaceAll(",", "."));
                        resultArea.setText(calculator.getBuffer());
                    } else {
                        switch(key) {
                            case '+':
                                calculator.setOperation(Operation.ADDITION);
                                break;
                            case '-':
                                calculator.setOperation(Operation.SUBTRACTION);
                                break;
                            case '*':
                                calculator.setOperation(Operation.MULTIPLICATION);
                                break;
                            case '/':
                                calculator.setOperation(Operation.DIVISION);
                                break;
                            case '=':
                            case KeyEvent.VK_ENTER:
                                resultArea.setText(Util.formatValue(String.valueOf(calculator.calculate())));
                                break;
                            case KeyEvent.VK_BACK_SPACE:
                                calculator.reset();
                                resultArea.setText(calculator.getBuffer());
                                break;
                        }
                    }
                }
            }
        }, AWTEvent.KEY_EVENT_MASK);
    }

    private void buildLayout() {
        resultArea = new JTextField("0");
        resultArea.setEnabled(false);
        resultArea.setDisabledTextColor(Color.BLACK);
        resultArea.setHorizontalAlignment(SwingConstants.RIGHT);
        resultArea.setFont(resultArea.getFont().deriveFont((float)30));
        positionElement(0, 0, 4, 1, 1.0, resultArea);

        cancelBtn = new JButton("C");
        positionElement(0, 1, 2, 1, 1.0, cancelBtn);

        pnBtn = new JButton("+/-");
        positionElement(2, 1, 1, 1, 1.0, pnBtn);

        divideBtn = new OperationButton("÷", Operation.DIVISION);
        positionElement(3, 1, 1, 1, 1.0, divideBtn);

        timesBtn = new OperationButton("X", Operation.MULTIPLICATION);
        positionElement(3, 2, 1, 1, 1.0, timesBtn);

        minusBtn = new OperationButton("-", Operation.SUBTRACTION);
        positionElement(3, 3, 1, 1, 1.0, minusBtn);

        plusBtn = new OperationButton("+", Operation.ADDITION);
        positionElement(3, 4, 1, 1, 1.0, plusBtn);

        equalsBtn = new JButton("=");
        positionElement(3, 5, 1, 1, 1.0, equalsBtn);

        commaBtn = new JButton(".");
        positionElement(2, 5, 1, 1, 1.0, commaBtn);

        bindActions();
        generateNumLayout();
    }

    private void bindActions() {
        cancelBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculator.reset();
                resultArea.setText(calculator.getBuffer());
            }
        });
        pnBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                calculator.invertSign();
                resultArea.setText(Util.formatValue(String.valueOf(calculator.getBuffer())));
            }
        });
        divideBtn.addActionListener(new OperationAction());
        timesBtn.addActionListener(new OperationAction());
        minusBtn.addActionListener(new OperationAction());
        plusBtn.addActionListener(new OperationAction());
        equalsBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double res = calculator.calculate();
                resultArea.setText(Util.formatValue(String.valueOf(res)));
            }
        });
        commaBtn.addActionListener(new ValueAction(resultArea));
    }

    private void generateNumLayout() {
        numBtns = new JButton[10];
        int row = 4;
        int col = 0;
        numBtns[0] = new JButton("0");
        numBtns[0].addActionListener(new ValueAction(resultArea));
        positionElement(0, 5, 2, 1, 1.0, numBtns[0]);
        for(int i = 1; i < numBtns.length; i++) {
            numBtns[i] = new JButton(String.valueOf(i));
            numBtns[i].addActionListener(new ValueAction(resultArea));
            positionElement(col, row, 1, 1, 1.0, numBtns[i]);
            if(col < 2)
                col++;
            else {
                col = 0;
                row--;
            }
        }
    }

    private void positionElement(int x, int y, int xX, int xY, double wY, JComponent c) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.gridwidth = xX;
        constraints.gridheight = xY;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weighty = wY;
        constraints.weightx = wY;
        this.getContentPane().add(c, constraints);
        constraints.weighty = 0.0;
        constraints.weightx = 0.0;
    }
}