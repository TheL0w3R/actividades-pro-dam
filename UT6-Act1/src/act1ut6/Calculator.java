package act1ut6;

import act1ut6.Utilities.Operation;

/**
 * Created by thel0w3r on 23/3/18.
 * All Rights Reserved.
 */
public class Calculator {

    private static Calculator instance;
    private String buffer;
    private double temp;
    private Operation op;
    private boolean afterOp;

    public Calculator() {
        instance = this;
        this.buffer = "0";
        this.temp = 0;
        this.op = Operation.NONE;
        this.afterOp = false;
    }

    public static Calculator getInstance() {
        return instance;
    }

    public String getBuffer() {
        return buffer;
    }

    public void addToBuffer(String val) {
        if(afterOp)
            buffer = "0";
        if(!buffer.equalsIgnoreCase("0")) {
            if (val.equalsIgnoreCase(".")) {
                if (buffer.split("\\.").length <= 1)
                    buffer += val;
            } else
                buffer += val;
        } else {
            if(val.equalsIgnoreCase("."))
                buffer += val;
            else
                buffer = val;
        }
        afterOp = false;
    }

    public double getTemp() {
        return temp;
    }

    public double calculate() {
        double r = 0;
        switch(this.op) {
            case NONE:
                r = Double.parseDouble(buffer);
                break;
            case ADDITION:
                r = temp + Double.valueOf(buffer);
                break;
            case SUBTRACTION:
                r = temp - Double.valueOf(buffer);
                break;
            case MULTIPLICATION:
                r = temp * Double.valueOf(buffer);
                break;
            case DIVISION:
                r = temp / Double.valueOf(buffer);
                break;
        }
        this.op = Operation.NONE;
        buffer = String.valueOf(r);
        temp = 0;
        afterOp = true;
        return r;
    }

    public void reset() {
        this.buffer = "0";
        this.temp = 0;
        this.op = Operation.NONE;
    }

    public void invertSign() {
        if(buffer.startsWith("-"))
            buffer = buffer.substring(1);
        else
            buffer = "-" + buffer;
    }

    public void setOperation(Operation op) {
        this.op = op;
        temp = Double.valueOf(buffer);
        buffer = "0";
    }
}
