package act1ut6.Utilities;

/**
 * Created by thel0w3r on 23/3/18.
 * All Rights Reserved.
 */
public class Util {

    public static String formatValue(String v) {
        String[] splitted = v.split("\\.");
        if(splitted.length > 1) {
            if(splitted[1] != null) {
                if (splitted[1].equalsIgnoreCase("0"))
                    return splitted[0];
            }
        }
        return v;
    }

}
