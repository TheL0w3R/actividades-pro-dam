package act1ut6.Utilities;

import javax.swing.*;

/**
 * Created by thel0w3r on 23/3/18.
 * All Rights Reserved.
 */
public class OperationButton extends JButton {

    private Operation op;

    public OperationButton(String text, Operation op) {
        super(text);
        this.op = op;
    }

    public Operation getOp() {
        return op;
    }
}
