package act1ut6.Utilities;

/**
 * Created by thel0w3r on 23/3/18.
 * All Rights Reserved.
 */
public enum Operation {
    ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION, NONE
}
