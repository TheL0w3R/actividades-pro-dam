package act1ut6.Actions;

import act1ut6.Calculator;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by thel0w3r on 23/3/18.
 * All Rights Reserved.
 */
public class ValueAction implements ActionListener {

    private JTextField resultArea;

    public ValueAction(JTextField resultArea) {
        this.resultArea = resultArea;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Calculator c = Calculator.getInstance();
        c.addToBuffer(((JButton) e.getSource()).getText());
        resultArea.setText(c.getBuffer());
    }
}
