package act1ut6.Actions;

import act1ut6.Calculator;
import act1ut6.Utilities.OperationButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by thel0w3r on 23/3/18.
 * All Rights Reserved.
 */
public class OperationAction implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() instanceof OperationButton) {
            Calculator.getInstance().setOperation(((OperationButton) e.getSource()).getOp());
        }
    }
}
