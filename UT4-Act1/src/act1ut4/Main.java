package act1ut4;

import act1ut4.ejs.Ejercicio1;
import act1ut4.ejs.Ejercicio2;
import com.thel0w3r.tlmenuframework.Locale;
import com.thel0w3r.tlmenuframework.Menu;
import com.thel0w3r.tlmenuframework.MenuBuilder;

/**
 * Created by TheL0w3R on 24/11/2017.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        Menu m = new MenuBuilder()
                .title("Actividad 1 UT4 [Lorenzo Pinna Rodríguez]")
                .setLocale(Locale.ES)
                .addOption(new Ejercicio1())
                .addOption(new Ejercicio2())
                .build();
        m.start();
    }

}
