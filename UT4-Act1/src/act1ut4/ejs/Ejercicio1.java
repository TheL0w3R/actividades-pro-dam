package act1ut4.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 24/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio1 extends MenuOption {

    public Ejercicio1() {
        super("Ejercicio 1");
    }

    @Override
    public void run() {
        System.out.println("Escribe un comando, seguido de ':' y un espacio: ");
        String input = sc.nextLine().trim();
        if(input.contains(":")) {
            String cmd = input.substring(0, input.indexOf(':'));
            String data = input.substring(input.indexOf(':') + 2);
            if(cmd.equalsIgnoreCase("invertir")) {
                System.out.println(mayus(reverse(data)));
            } else if(cmd.equalsIgnoreCase("nombre")) {
                System.out.println("Hola " + data);
            } else if(cmd.equalsIgnoreCase("escribir")) {
                System.out.println(data.replaceAll("esta", "una"));
            }
        } else {
            System.out.println("Invalid command");
        }
    }

    private String reverse(String s) {
        String tmp = "";
        for(int i = s.length() -1; i >= 0; i--) {
            tmp = tmp.concat(String.valueOf(s.charAt(i)));
        }
        return tmp;
    }

    private String mayus(String s) {
        String tmp = "";
        for(int i = 0; i < s.length(); i++) {
            tmp = tmp.concat(String.valueOf(Character.toUpperCase(s.charAt(i))));
        }
        return tmp;
    }
}
