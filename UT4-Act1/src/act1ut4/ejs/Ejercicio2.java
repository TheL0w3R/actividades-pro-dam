package act1ut4.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 20/2/18.
 * All Rights Reserved.
 */
public class Ejercicio2 extends MenuOption {

    public Ejercicio2() {
        super("Ejercicio 2");
    }

    @Override
    public void run() {
        System.out.print("Escribe el título: ");
        StringBuffer title = new StringBuffer(sc.nextLine());
        System.out.print("Escribe el autor: ");
        StringBuffer author = new StringBuffer(sc.nextLine());

        title.append(", ").append(author);
        System.out.println(title);
        title.reverse();
        System.out.println(title);
        title.reverse().replace(title.indexOf(","), title.indexOf(",") + 2, " escrito por ");
        System.out.println(title);
    }
}
