package act3ut5;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by TheL0w3R on 08/03/2018.
 * All Rights Reserved.
 */
public class Util {

    private static Scanner sc = new Scanner(System.in);

    public static double readDouble(String msg) {
        double tmp;
        while(true) {
            System.out.print(msg);

            try {
                tmp = sc.nextDouble();
                sc.nextLine();
                break;
            } catch (InputMismatchException e) {
                System.out.println("Tipo de dato incorrecto.");
            }
        }
        return tmp;
    }

    public static double readOrExit(String msg) {
        double tmp;
        while(true) {
            System.out.print(msg);

            try {
                String in = sc.nextLine();
                if(in.equalsIgnoreCase("exit"))
                    System.exit(0);
                tmp = Double.parseDouble(in);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Tipo de dato incorrecto.");
            }
        }
        return tmp;
    }

}
