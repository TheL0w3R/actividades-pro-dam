package act3ut5;

/**
 * Created by TheL0w3R on 08/03/2018.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        Alarma al = new Alarma(30, 100);
        al.setLimiteTemperatura(Util.readDouble("Introduce el límite de la temperatura: "));

        while(true) {
            al.setTemperatura(Util.readOrExit("Introduce la temperatura actual: "));
        }
    }


}
