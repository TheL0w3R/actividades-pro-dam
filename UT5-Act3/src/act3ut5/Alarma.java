package act3ut5;

/**
 * Created by TheL0w3R on 08/03/2018.
 * All Rights Reserved.
 */
public class Alarma {

    private double temperatura, limiteTemperatura;
    private Timbre timbre;

    public Alarma(double temperatura) {
        this.temperatura = temperatura;
        this.limiteTemperatura = 100D;
        this.timbre = new Timbre();
    }

    public Alarma(double temperatura, double limiteTemperatura) {
        this.temperatura = temperatura;
        this.limiteTemperatura = limiteTemperatura;
        this.timbre = new Timbre();
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
        checkTemp();
    }

    public double getLimiteTemperatura() {
        return limiteTemperatura;
    }

    public void setLimiteTemperatura(double limiteTemperatura) {
        this.limiteTemperatura = limiteTemperatura;
        checkTemp();
    }

    public void checkTemp() {
        if(temperatura > limiteTemperatura)
            timbre.setActive();
        else
            timbre.deactivate();
    }
}
