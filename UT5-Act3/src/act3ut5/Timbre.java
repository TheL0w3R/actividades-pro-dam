package act3ut5;

/**
 * Created by TheL0w3R on 08/03/2018.
 * All Rights Reserved.
 */
public class Timbre {

    private boolean active;

    public Timbre() {
        this.active = false;
    }

    public void setActive() {
        this.active = true;
        System.out.println("Límite de temperatura sobrepasado!!");
    }

    public void deactivate() {
        this.active = false;
        System.out.println("Alarma apagada.");
    }

}
