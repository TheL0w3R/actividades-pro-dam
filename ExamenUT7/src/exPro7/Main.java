package exPro7;

import com.thel0w3r.tlmenuframework.Locale;
import com.thel0w3r.tlmenuframework.Menu;
import com.thel0w3r.tlmenuframework.MenuBuilder;
import exPro7.menuoptions.AddProduct;
import exPro7.menuoptions.ListProducts;
import exPro7.menuoptions.SellProduct;

/**
 * Created by thel0w3r on 2/5/18.
 * All Rights Reserved.
 */
public class Main {

    //ES NECESARIO INCLUIR COMO DEPENDENCIA EL ARCHIVO DENTRO DEL DIRECTORIO "/deps" ENTREGADO JUNTO CON EL EXAMEN.
    public static void main(String[] args) {
        ProductManager pm = new ProductManager();
        Menu m = new MenuBuilder()
                .title("Gestión de una Carpintería.")
                .setLocale(Locale.ES)
                .addOption(new AddProduct())
                .addOption(new ListProducts())
                .addOption(new SellProduct())
                .build();

        m.start();
    }

}
