package exPro7;

import exPro7.products.HandMade;
import exPro7.products.Kit;

import java.util.ArrayList;

/**
 * Created by thel0w3r on 2/5/18.
 * All Rights Reserved.
 */
public class ProductManager {

    private static ProductManager instance;

    private ArrayList<Product> products;

    public ProductManager() {
        instance = this;
        products = new ArrayList<>();
    }

    public static ProductManager getInstance() {
        return instance;
    }

    public void addProduct(Product p) {
        this.products.add(p);
    }

    public Product[] getProducts() {
        return products.toArray(new Product[]{});
    }

    public Kit[] getKits() {
        ArrayList<Kit> kits = new ArrayList<>();
        for(Product p : products) {
            if(p instanceof Kit)
                kits.add((Kit) p);
        }
        return kits.toArray(new Kit[]{});
    }

    public HandMade[] getHandMade() {
        ArrayList<HandMade> handMades = new ArrayList<>();
        for(Product p : products) {
            if(p instanceof HandMade)
                handMades.add((HandMade) p);
        }
        return handMades.toArray(new HandMade[]{});
    }
}
