package exPro7;

import java.util.Random;

/**
 * Created by thel0w3r on 2/5/18.
 * All Rights Reserved.
 */
public abstract class Product {

    private String id;
    protected String name;
    protected double price;
    protected int stock;

    public Product(String name, double price, int stock) {
        this.id = genID();
        this.name = name;
        this.price = price;
        this.stock = stock;
    }

    public void sell(int quantity) throws OutOfStockException {
        if(stock >= quantity)
            stock -= quantity;
        else
            throw new OutOfStockException();
    }

    private String genID() {
        String letters = "ABCDEFG";
        StringBuilder sb = new StringBuilder();
        sb.append(letters.charAt(new Random().nextInt(letters.length())));
        for(int i = 0; i < 3; i++) {
            sb.append(new Random().nextInt(9 + 1));
        }
        return sb.toString();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getStock() {
        return stock;
    }
}
