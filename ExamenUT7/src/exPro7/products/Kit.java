package exPro7.products;

import exPro7.ICost;
import exPro7.Product;

/**
 * Created by thel0w3r on 2/5/18.
 * All Rights Reserved.
 */
public class Kit extends Product implements ICost {

    private double buildCost;

    public Kit(String name, double price, int stock, double buildCost) {
        super(name, price, stock);
        this.buildCost = buildCost;
    }

    @Override
    public double getCost() {
        double p = this.price + buildCost;
        return p + ((p * 12) / 100);
    }

    public double getBuildCost() {
        return buildCost;
    }
}
