package exPro7.products;

import exPro7.ICost;
import exPro7.Product;

/**
 * Created by thel0w3r on 2/5/18.
 * All Rights Reserved.
 */
public class HandMade extends Product implements ICost {

    private String providerName;

    public HandMade(String name, double price, int stock, String providerName) {
        super(name, price, stock);
        this.providerName = providerName;
    }

    @Override
    public double getCost() {
        return this.price + ((this.price * 7) / 100);
    }

    public String getProviderName() {
        return providerName;
    }
}
