package exPro7.factories;

import exPro7.products.Kit;

/**
 * Created by thel0w3r on 2/5/18.
 * All Rights Reserved.
 */
public class KitFactory {

    private String name;
    private double price;
    private int stock;
    private double buildCost;

    public KitFactory() {
        this.name = "";
        this.price = 0;
        this.stock = 0;
        this.buildCost = 0;
    }

    public KitFactory setName(String name) {
        this.name = name;
        return this;
    }

    public KitFactory setPrice(double price) {
        this.price = price;
        return this;
    }

    public KitFactory setStock(int stock) {
        this.stock = stock;
        return this;
    }

    public KitFactory setBuildCost(double buildCost) {
        this.buildCost = buildCost;
        return this;
    }

    public Kit build() {
        return new Kit(name, price, stock, buildCost);
    }

}
