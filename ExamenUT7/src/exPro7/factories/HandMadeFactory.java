package exPro7.factories;

import exPro7.products.HandMade;
import exPro7.products.Kit;

/**
 * Created by thel0w3r on 2/5/18.
 * All Rights Reserved.
 */
public class HandMadeFactory {

    private String name;
    private double price;
    private int stock;
    private String providerName;

    public HandMadeFactory() {
        this.name = "";
        this.price = 0;
        this.stock = 0;
        this.providerName = "";
    }

    public HandMadeFactory setName(String name) {
        this.name = name;
        return this;
    }

    public HandMadeFactory setPrice(double price) {
        this.price = price;
        return this;
    }

    public HandMadeFactory setStock(int stock) {
        this.stock = stock;
        return this;
    }

    public HandMadeFactory setProviderName(String providerName) {
        this.providerName = providerName;
        return this;
    }

    public HandMade build() {
        return new HandMade(name, price, stock, providerName);
    }

}
