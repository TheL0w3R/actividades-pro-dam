package exPro7.menuoptions;

import com.thel0w3r.tlmenuframework.Locale;
import com.thel0w3r.tlmenuframework.Menu;
import com.thel0w3r.tlmenuframework.MenuBuilder;
import com.thel0w3r.tlmenuframework.MenuOption;
import exPro7.factories.HandMadeFactory;
import exPro7.factories.KitFactory;
import exPro7.ProductManager;
import exPro7.products.HandMade;
import exPro7.products.Kit;

/**
 * Created by thel0w3r on 2/5/18.
 * All Rights Reserved.
 */
public class AddProduct extends MenuOption {

    public AddProduct() {
        super("Añadir un mueble nuevo");
    }

    @Override
    public void run() {
        Menu submenu = new MenuBuilder()
                .title("Añadir mueble")
                .setLocale(Locale.ES)
                .addOption(new MenuOption("Kit") {
                    @Override
                    public void run() {
                        try {
                            KitFactory kf = new KitFactory();
                            System.out.print(" - Nombre del producto: ");
                            kf.setName(this.sc.nextLine());
                            System.out.print(" - Precio del producto: ");
                            kf.setPrice(this.sc.nextDouble());
                            System.out.print(" - Cantidad de productos: ");
                            kf.setStock(this.sc.nextInt());
                            System.out.print(" - Coste del montaje: ");
                            kf.setBuildCost(this.sc.nextDouble());
                            Kit k = kf.build();
                            ProductManager.getInstance().addProduct(k);
                            System.out.println("\n El mueble " + k.getName() + " con ID " + k.getId() + " ha sido creado con éxito.");
                            this.sc.nextLine();
                        } catch (Exception e) {
                            System.out.println("El tipo de dato especificado no es correcto.");
                        }
                    }
                })
                .addOption(new MenuOption("Hecho a mano") {
                    @Override
                    public void run() {
                        try {
                            HandMadeFactory hmf = new HandMadeFactory();
                            System.out.print(" - Nombre del producto: ");
                            hmf.setName(this.sc.nextLine());
                            System.out.print(" - Precio del producto: ");
                            hmf.setPrice(this.sc.nextDouble());
                            System.out.print(" - Cantidad de productos: ");
                            hmf.setStock(this.sc.nextInt());
                            this.sc.nextLine();
                            System.out.print(" - Nombre del artesano: ");
                            hmf.setProviderName(this.sc.nextLine());
                            HandMade hm = hmf.build();
                            ProductManager.getInstance().addProduct(hm);
                            System.out.println("\n El mueble " + hm.getName() + " con ID " + hm.getId() + " ha sido creado con éxito.");
                        } catch (Exception e) {
                            System.out.println("El tipo de dato especificado no es correcto.");
                        }
                    }
                })
                .selectMsg("Selecciona el tipo de mueble")
                .exitNum(3)
                .exitOption("Volver")
                .borderChar('#')
                .build();
        submenu.start();
    }
}
