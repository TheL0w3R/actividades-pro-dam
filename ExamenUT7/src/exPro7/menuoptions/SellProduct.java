package exPro7.menuoptions;

import com.thel0w3r.tlmenuframework.Locale;
import com.thel0w3r.tlmenuframework.Menu;
import com.thel0w3r.tlmenuframework.MenuBuilder;
import com.thel0w3r.tlmenuframework.MenuOption;
import exPro7.OutOfStockException;
import exPro7.Product;
import exPro7.ProductManager;
import exPro7.products.HandMade;
import exPro7.products.Kit;

import java.util.InputMismatchException;

/**
 * Created by thel0w3r on 2/5/18.
 * All Rights Reserved.
 */
public class SellProduct extends MenuOption {

    public SellProduct() {
        super("Vender muebles");
    }

    @Override
    public void run() {
        ProductManager pm = ProductManager.getInstance();
        MenuBuilder submenu = new MenuBuilder()
                .title("Vender muebles")
                .setLocale(Locale.ES)
                .selectMsg("Selecciona el mueble que desee vender")
                .exitNum(pm.getProducts().length + 1)
                .exitOption("Volver")
                .borderChar('#');

        for(Product p : pm.getProducts()) {
            submenu.addOption(new MenuOption("[" + p.getId() + "] " + p.getName()) {
                @Override
                public void run() {
                    System.out.println(" > Tipo de mueble: " + ((p instanceof Kit) ? "Kit" : "Hecho a mano"));
                    System.out.println(" > Nombre del mueble: " + p.getName());
                    System.out.println(" > Coste total del mueble: " + ((p instanceof Kit) ? ((Kit) p).getCost() : ((HandMade) p).getCost()) + "€");
                    System.out.println(" > Stock restante: " + p.getStock());
                    if(p instanceof Kit)
                        System.out.println(" > Coste del montaje: " + ((Kit) p).getBuildCost());
                    else
                        System.out.println(" > Nombre del artesano: " + ((HandMade) p).getProviderName());

                    try {
                        System.out.print("\nUnidades a vender: ");
                        p.sell(sc.nextInt());
                        System.out.println("El Stock actual del producto es de " + p.getStock());
                    } catch (InputMismatchException e) {
                        System.out.println("No es un valo válido.");
                    } catch (OutOfStockException e) {
                        System.out.println("No hay suficientes unidades.");
                    }
                }
            });
        }

        if(pm.getProducts().length > 0)
            submenu.build().start();
        else
            System.out.println("No hay muebles disponibles.");
    }
}
