package exPro7.menuoptions;

import com.thel0w3r.tlmenuframework.MenuOption;
import exPro7.Product;
import exPro7.ProductManager;
import exPro7.products.HandMade;
import exPro7.products.Kit;

/**
 * Created by thel0w3r on 2/5/18.
 * All Rights Reserved.
 */
public class ListProducts extends MenuOption {

    public ListProducts() {
        super("Ver productos registrados");
    }

    @Override
    public void run() {
        System.out.println("Muebles de tipo kit: ");
        for (Kit k : ProductManager.getInstance().getKits()) {
            System.out.println(" - [" + k.getId() + "] " + k.getName() + " (" + k.getCost() + "€) " + k.getStock() + " unidades en stock");
        }

        System.out.println("Muebles hechos a mano: ");
        for (HandMade hm : ProductManager.getInstance().getHandMade()) {
            System.out.println(" - [" + hm.getId() + "] " + hm.getName() + " by " + hm.getProviderName() + " (" + hm.getCost() + "€) " + hm.getStock() + " unidades en stock");
        }
    }
}
