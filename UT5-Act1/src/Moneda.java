/**
 * Created by thel0w3r on 7/3/18.
 * All Rights Reserved.
 */
public class Moneda {

    private final double exchange;

    public Moneda() {
        exchange = 1.24059D;
    }

    public Moneda(double exchange) {
        this.exchange = exchange;
    }

    public double dollarEuro(double dollar) {
        return dollar/exchange;
    }

    public double euroDollar(double euro) {
        return euro*exchange;
    }
}
