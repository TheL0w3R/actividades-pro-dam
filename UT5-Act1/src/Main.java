/**
 * Created by thel0w3r on 7/3/18.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        Moneda m1 = new Moneda();
        System.out.println("$5.9 en euros: " + m1.dollarEuro(5.9));
        System.out.println("5.9€ en dólares: " + m1.euroDollar(5.9));

        Moneda m2 = new Moneda(7.3);
        System.out.println("$5.9 en euros: " + m2.dollarEuro(5.9));
        System.out.println("5.9€ en dólares: " + m2.euroDollar(5.9));
    }

}
