import java.util.*;

/**
 * Created by thel0w3r on 5/3/18.
 * All Rights Reserved.
 */
public class Act4UT4 {

    private Scanner sc;
    private ArrayList<Double> heights;

    private Act4UT4() {
        sc = new Scanner(System.in);
        heights = new ArrayList<>();
    }

    public static void main(String[] args) {
        new Act4UT4().init();
    }

    private void init() {
        boolean exit = false;
        int amount = 1;
        while(!exit) {
            try {
                System.out.print("Introduce la altura del " + amount + "º alumno (negativo para salir): ");
                double h = sc.nextDouble();
                if(h < 0)
                    exit = true;
                else {
                    heights.add(h);
                    amount++;
                }
            } catch(InputMismatchException e) {
                sc.nextLine();
                System.out.println("El dato introducido no es correcto.");
            }
        }
        printHeights();
        double med = getMed();
        System.out.println("\nLa media de alturas es: " + med);
        Collections.sort(heights);
        System.out.println("\nHay " + getBelowMed(med) + " alumnos por debajo de la media.");
        System.out.println("\n" + ((getMatching(med)) ?
                "La altura de uno o varios alumnos es exactamente la misma que la media." :
                "No existe ningún alumno cuya altura coincida exactamente con la media"));
    }

    private void printHeights() {
        Double[] hVector = heights.toArray(new Double[heights.size()]);
        int student = 1;

        System.out.println("\nAlturas registradas:");
        for(double h : hVector) {
            System.out.println("Alumno " + student + ": " + h);
            student++;
        }
    }

    private double getMed() {
        double sum = 0;
        for(double h : heights)
            sum += h;
        return Math.round((sum/heights.size()) * 10D) / 10D;
    }

    private int getBelowMed(double med) {
        int am = 0;
        for(double h : heights)
            if(h < med) am++;
        return am;
    }

    private boolean getMatching(double med) {
        boolean match = false;
        for(double h : heights)
            if(h == med) match = true;
        return match;
    }

}
