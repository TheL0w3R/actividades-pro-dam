package act3ut4.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

import java.text.DecimalFormat;
import java.util.Random;

/**
 * Created by TheL0w3R on 08/01/2018.
 * All Rights Reserved.
 */
public class Ejercicio1 extends MenuOption {

    public Ejercicio1() {
        super("Ejercicio 1");
    }

    @Override
    public void run() {
        System.out.print("Escribe el numero de alumnos: ");
        int sAmount = sc.nextInt();
        String[][] data = new String[sAmount + 1][5];
        data[0][0] = "Nombre";
        data[0][1] = "PRO";
        data[0][2] = "BAE";
        data[0][3] = "SSF";
        data[0][4] = "LND";
        sc.nextLine();
        System.out.print("Quieres generar las notas y nombres de manera aleatoria? (S/n): ");
        String s = sc.nextLine();
        if(s.equalsIgnoreCase("n")) {
            for (int i = 1; i < data.length; i++) {
                if(i != 1)
                    sc.nextLine();
                for (int k = 0; k < data[0].length; k++) {
                    if (k == 0) {
                        System.out.print("Escribe el nombre del " + i + "º alumno: ");
                        data[i][k] = sc.nextLine();
                    } else {
                        System.out.print("Nota de " + data[0][k] + " para " + data[i][0] + ": ");
                        data[i][k] = String.valueOf(sc.nextDouble());
                    }
                }
            }
        } else if(s.equalsIgnoreCase("s")) {
            for(int i = 1; i < data.length; i++) {
                for(int k = 0; k < data[0].length; k++) {
                    if(k == 0)
                        data[i][0] = "Alumno " + i;
                    else
                        data[i][k] = String.valueOf(randomMark());
                }
            }
        }
        printTable(data, "Datos actuales de los alumnos");
        menuHandler(data);
    }

    private double randomMark() {
        Random r = new Random();
        return (r.nextInt((9)*10+1)+10) / 10.00;
    }

    private void menuHandler(String[][] data) {
        String options = "\n 1 - Media por módulo\n 2 - Media por alumno\n 99 - Salir\n\nElige una opción válida: ";
        boolean finished = false;
        while(!finished) {
            System.out.print(options);
            int option = sc.nextInt();
            System.out.println();
            switch(option) {
                case 1:
                    System.out.print(" 1 - PRO\n 2 - BAE\n 3 - SSF\n 4 - LND\n 5 - Todos los módulos\n\nElige un módulo: ");
                    int sig = sc.nextInt();
                    medByModule(data, sig);
                    break;
                case 2:
                    for(int i = 1; i < data.length; i++) {
                        System.out.println(" " + i + " - " + data[i][0]);
                    }
                    System.out.println(" " + data.length + " - Todos los alumnos");
                    System.out.print("\nElige un alumno: ");
                    int student = sc.nextInt();
                    medByStudent(data, student);
                    break;
                case 99:
                    finished = true;
                    break;
            }
        }
    }

    private void medByModule(String[][] data, int sig) {
        if(sig == 5) {
            double[] sums = {0,0,0,0};
            String[][] d = new String[2][(data[0].length - 1)];
            for(int i = 1; i < data.length; i++) {
                for(int k = 0; k < sums.length; k++) {
                    sums[k] += Double.parseDouble(data[i][k+1]);
                }
            }
            for(int i = 1; i < data[0].length; i++) {
                d[0][i - 1] = data[0][i];
            }
            for(int i = 0; i < sums.length; i++) {
                d[1][i] = String.valueOf(Math.round((sums[i]/(data.length - 1)) * 10D) / 10D);
            }
            printTable(d, "Media de todos los modulos");
        } else {
            double sum = 0;
            for(int i = 1; i < data.length; i++) {
                sum += Double.parseDouble(data[i][sig]);
            }
            System.out.println("\nLa media de " + data[0][sig] + " es " + Math.round((sum/(data.length - 1)) * 10D) / 10D);
        }
    }

    private void medByStudent(String[][] data, int studentIndex) {
        if(studentIndex == data.length) {
            String[][] allData = new String[data.length][2];
            allData[0][0] = "Alumno";
            allData[0][1] = "Media";
            for(int i = 1; i < data.length; i++) {
                allData[i][0] = data[i][0];
                double sum = 0;
                for(int k = 1; k < data[0].length; k++) {
                    sum += Double.parseDouble(data[i][k]);
                }
                allData[i][1] = String.valueOf(Math.round((sum/(data[0].length - 1)) * 10D) / 10D);
            }
            printTable(allData, "Medias por alumno");
        } else {
            double sum = 0;
            for(int i = 1; i < data[0].length; i++) {
                sum += Double.parseDouble(data[studentIndex][i]);
            }
            System.out.println("\nLa media de '" + data[studentIndex][0] + "' es " + Math.round((sum/(data[0].length - 1)) * 10D) / 10D);
        }
    }

    private void printTable(String[][] data, String title) {
        StringBuilder sb = new StringBuilder("\n");
        int[] rowSpaces = new int[data[0].length];
        for(String[] row : data) {
            for(int i = 0; i < row.length; i++) {
                if(row[i].length() > (rowSpaces[i] - 4)) {
                    rowSpaces[i] = row[i].length() + 4;
                }
            }
        }

        int totalSpaces = 2;

        for(int spacer : rowSpaces) {
            totalSpaces += spacer;
        }
        int titleSpacer = totalSpaces - title.length();
        sb.append(getStr(titleSpacer/2, " ")).append(title);
        if(titleSpacer % 2 == 0)
            sb.append(getStr(titleSpacer/2, " "));
        else
            sb.append(getStr((titleSpacer/2) + 1, " "));
        sb.append("\n").append(getStr(totalSpaces, "=")).append("\n");
        for(String[] row : data) {
            sb.append("=");
            for(int i = 0; i < row.length; i++) {
                int currentSpacing = rowSpaces[i] - row[i].length();
                String spacer = getStr(currentSpacing/2, " ");
                if(currentSpacing % 2 == 0) {
                    //SPACING IS PAIR
                    sb.append(spacer).append(row[i]).append(spacer);
                } else {
                    //SPACING IS ODD
                    sb.append(spacer).append(row[i]).append(getStr((currentSpacing/2) + 1, " "));
                }
            }
            sb.append("=\n");
        }
        sb.append(getStr(totalSpaces, "=")).append("\n");
        System.out.println(sb);
    }

    private String getStr(int l, String c) {
        return (new String(new char[l])).replace("\u0000", c);
    }
}
