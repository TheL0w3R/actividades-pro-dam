import java.util.ArrayList;
import java.util.Collections;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by thel0w3r on 5/3/18.
 * All Rights Reserved.
 */
public class Act5UT4 {

    private Scanner sc;
    private char[] separator;
    private ArrayList<String> section1, section2, section3, section4;

    private Act5UT4() {
        sc = new Scanner(System.in);
        separator = new char[4];
        section1 = new ArrayList<>();
        section2 = new ArrayList<>();
        section3 = new ArrayList<>();
        section4 = new ArrayList<>();
    }

    public static void main(String[] args) {
        new Act5UT4().init();
    }

    private void init() {
        initSeparator();

        int am = 1;
        boolean exit = false;

        while (!exit) {
            String student = readString("Introduce el nombre completo del " + am + "º alumno (EXIT para salir): ",
                    ",",
                    "Formato correcto: <apellidos>, <nombre>");

            if(student.equalsIgnoreCase("exit"))
                exit = true;
            else {
                addStudent(student);
                am++;
            }
        }

        while(true) {
            sortLists();

            System.out.println("La lista con rango " + separator[0] + " - " + separator[1] + " cuenta con " + section1.size() + " alumnos.");
            for(String s1 : section1)
                System.out.println(" - " + formatStudent(s1));

            System.out.println("La lista con rango " + separator[1] + " - " + separator[2] + " cuenta con " + section2.size() + " alumnos.");
            for(String s2 : section2)
                System.out.println(" - " + formatStudent(s2));

            System.out.println("La lista con rango " + separator[2] + " - " + separator[3] + " cuenta con " + section3.size() + " alumnos.");
            for(String s3 : section3)
                System.out.println(" - " + formatStudent(s3));

            System.out.println("La lista con rango " + separator[3] + " - Z cuenta con " + section4.size() + " alumnos.");
            for(String s4 : section4)
                System.out.println(" - " + formatStudent(s4));

            String op = readString("\nQuieres rectificar algún error? (S/n): ", null, null);
            if (op.equalsIgnoreCase("s"))
                editMenu();
            else if (op.equalsIgnoreCase("n"))
                break;
        }
    }

    private void initSeparator() {
        System.out.println("Establezca los rangos para almacenar los alumnos: ");
        for(int i = 0; i < separator.length; i++) {
            separator[i] = Character.toUpperCase(readString("Principio del rango para la " + (i+1) + "º categoría: ", null, null).charAt(0));
        }
    }

    private String readString(String readMsg, String contains, String containsMsg) {
        String tmp;
        do {
            try {
                System.out.print(readMsg);
                tmp = sc.nextLine();
                if(contains != null) {
                    if(!tmp.contains(contains) && !tmp.equalsIgnoreCase("exit"))
                        throw new InputMismatchException();
                }
                break;
            } catch(InputMismatchException e) {
                System.out.println("El dato introducido no es válido.");
                if(contains != null)
                    System.out.println(containsMsg);
            }
        } while (true);
        return tmp;
    }

    private int categorize(String student) {
        char firstLetter = Character.toUpperCase(student.split(",")[0].trim().charAt(0));
        if(firstLetter >= separator[0] && firstLetter < separator[1]) {
            return 1;
        }
        else if(firstLetter >= separator[1] && firstLetter < separator[2]) {
            return 2;
        }
        else if(firstLetter >= separator[2] && firstLetter < separator[3]) {
            return 3;
        }
        else if(firstLetter >= separator[3] && firstLetter <= ((int)'Z')) {
            return 4;
        }
        return -1;
    }

    private String formatStudent(String student) {
        return student.split(",")[1].trim() + " " + student.split(",")[0].trim();
    }

    private void sortLists() {
        Collections.sort(section1);
        Collections.sort(section2);
        Collections.sort(section3);
        Collections.sort(section4);
    }

    private void editMenu() {
        StringBuilder sb = new StringBuilder("\n");
        ArrayList<String> union = new ArrayList<>();

        union.addAll(section1);
        union.addAll(section2);
        union.addAll(section3);
        union.addAll(section4);

        for(int i = 0; i < union.size(); i++) {
            sb.append(" ").append(i + 1).append(" ").append(formatStudent(union.get(i))).append("\n");
        }

        sb.append(" ").append(union.size() + 1).append(" Salir\n");

        boolean exit = false;
        while(!exit) {
            System.out.print(sb);
            int op = (readInt("Selecciona el alumno a modificar: ")) - 1;
            if(op >= 0 && op <= (union.size() - 1)) {
                String newStudent = readString("Introduce el nuevo valor para " + formatStudent(union.get(op)) + ": ",
                        ",",
                        "Formato correcto: <apellidos>, <nombre>");

                removeStudent(union.get(op));
                addStudent(newStudent);
                exit = true;
            } else
                System.out.println("No es una opción válida.");
        }
    }

    private void removeStudent(String student) {
        switch(categorize(student)) {
            case 1:
                section1.remove(student);
                break;
            case 2:
                section2.remove(student);
                break;
            case 3:
                section3.remove(student);
                break;
            case 4:
                section4.remove(student);
                break;
        }
    }

    private void addStudent(String student) {
        switch(categorize(student)) {
            case 1:
                section1.add(student);
                break;
            case 2:
                section2.add(student);
                break;
            case 3:
                section3.add(student);
                break;
            case 4:
                section4.add(student);
                break;
        }
    }

    private int readInt(String readMsg) {
        int tmp;
        do {
            try {
                System.out.print(readMsg);
                tmp = sc.nextInt();
                break;
            } catch(InputMismatchException e) {
                System.out.println("El dato introducido no es válido.");
            }
        } while (true);
        return tmp;
    }
}
