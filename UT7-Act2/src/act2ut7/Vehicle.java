package act2ut7;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 18/5/18.
 * All Rights Reserved.
 */
public abstract class Vehicle {

    protected String regNum;
    protected int numChassis;
    protected String brand;
    protected double power;
    protected String ownerDNI;
    protected LocalDate regDate;

    public Vehicle(String regNum, int numChassis, String brand, double power, String ownerDNI, LocalDate regDate) {
        this.regNum = regNum;
        this.numChassis = numChassis;
        this.brand = brand;
        this.power = power;
        this.ownerDNI = ownerDNI;
        this.regDate = regDate;
    }

    public abstract LocalDate itv();

    public String getRegNum() {
        return regNum;
    }

    public void setRegNum(String regNum) {
        this.regNum = regNum;
    }

    public int getNumChassis() {
        return numChassis;
    }

    public void setNumChassis(int numChassis) {
        this.numChassis = numChassis;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

    public String getOwnerDNI() {
        return ownerDNI;
    }

    public void setOwnerDNI(String ownerDNI) {
        this.ownerDNI = ownerDNI;
    }

    public LocalDate getRegDate() {
        return regDate;
    }

    public void setRegDate(LocalDate regDate) {
        this.regDate = regDate;
    }
}
