package act2ut7.factories;

import act2ut7.vehicles.Motorcycle;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 22/5/18.
 * All Rights Reserved.
 */
public class MotorcycleFactory {

    private int numChassis;
    private String regNum;
    private String brand;
    private double power;
    private String ownerDNI;
    private LocalDate regDate;

    public MotorcycleFactory setNumChassis(int numChassis) {
        this.numChassis = numChassis;
        return this;
    }

    public MotorcycleFactory setRegNum(String regNum) {
        this.regNum = regNum;
        return this;
    }

    public MotorcycleFactory setBrand(String brand) {
        this.brand = brand;
        return this;
    }

    public MotorcycleFactory setPower(double power) {
        this.power = power;
        return this;
    }

    public MotorcycleFactory setOwnerDNI(String ownerDNI) {
        this.ownerDNI = ownerDNI;
        return this;
    }

    public MotorcycleFactory setRegDate(LocalDate regDate) {
        this.regDate = regDate;
        return this;
    }

    public Motorcycle build() {
        return new Motorcycle(regNum, numChassis, brand, power, ownerDNI, regDate);
    }
}
