package act2ut7.factories;

import act2ut7.ServiceType;
import act2ut7.vehicles.Bus;
import act2ut7.vehicles.Motorcycle;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 22/5/18.
 * All Rights Reserved.
 */
public class BusFactory {

    private int numChassis;
    private String regNum;
    private String brand;
    private double power;
    private String ownerDNI;
    private LocalDate regDate;
    private int numSeats;
    private ServiceType service;

    public BusFactory setNumChassis(int numChassis) {
        this.numChassis = numChassis;
        return this;
    }

    public BusFactory setRegNum(String regNum) {
        this.regNum = regNum;
        return this;
    }

    public BusFactory setBrand(String brand) {
        this.brand = brand;
        return this;
    }

    public BusFactory setPower(double power) {
        this.power = power;
        return this;
    }

    public BusFactory setOwnerDNI(String ownerDNI) {
        this.ownerDNI = ownerDNI;
        return this;
    }

    public BusFactory setRegDate(LocalDate regDate) {
        this.regDate = regDate;
        return this;
    }

    public BusFactory setNumSeats(int numSeats) {
        this.numSeats = numSeats;
        return this;
    }

    public BusFactory setService(ServiceType service) {
        this.service = service;
        return this;
    }

    public Bus build() {
        return new Bus(regNum, numChassis, brand, power, ownerDNI, regDate, numSeats, service);
    }
}
