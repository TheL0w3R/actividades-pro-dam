package act2ut7.factories;

import act2ut7.vehicles.Motorcycle;
import act2ut7.vehicles.Tourism;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 22/5/18.
 * All Rights Reserved.
 */
public class TourismFactory {

    private int numChassis;
    private String regNum;
    private String brand;
    private double power;
    private String ownerDNI;
    private LocalDate regDate;
    private int numDoors;
    private boolean canTow;
    private int numSeats;

    public TourismFactory setNumChassis(int numChassis) {
        this.numChassis = numChassis;
        return this;
    }

    public TourismFactory setRegNum(String regNum) {
        this.regNum = regNum;
        return this;
    }

    public TourismFactory setBrand(String brand) {
        this.brand = brand;
        return this;
    }

    public TourismFactory setPower(double power) {
        this.power = power;
        return this;
    }

    public TourismFactory setOwnerDNI(String ownerDNI) {
        this.ownerDNI = ownerDNI;
        return this;
    }

    public TourismFactory setRegDate(LocalDate regDate) {
        this.regDate = regDate;
        return this;
    }

    public TourismFactory setNumDoors(int numDoors) {
        this.numDoors = numDoors;
        return this;
    }

    public TourismFactory setCanTow(boolean canTow) {
        this.canTow = canTow;
        return this;
    }

    public TourismFactory setNumSeats(int numSeats) {
        this.numSeats = numSeats;
        return this;
    }

    public Tourism build() {
        return new Tourism(regNum, numChassis, brand, power, ownerDNI, regDate, numDoors, canTow, numSeats);
    }
}
