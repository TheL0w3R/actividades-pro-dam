package act2ut7;

import act2ut7.vehicles.Bus;
import act2ut7.vehicles.Motorcycle;
import act2ut7.vehicles.Tourism;

import java.util.ArrayList;

/**
 * Created by thel0w3r on 22/5/18.
 * All Rights Reserved.
 */
public class VehicleManager {

    private static VehicleManager instance;

    private ArrayList<Vehicle> vehicles;

    public VehicleManager() {
        instance = this;

        vehicles = new ArrayList<>();
    }

    public static VehicleManager getInstance() {
        return instance;
    }

    public void addVehicle(Vehicle v) {
        vehicles.add(v);
    }

    public Vehicle[] getAll() {
        return vehicles.toArray(new Vehicle[]{});
    }

    public Motorcycle[] getMotorcycles() {
        ArrayList<Motorcycle> ms = new ArrayList<>();
        for(Vehicle v : vehicles) {
            if(v instanceof Motorcycle)
                ms.add((Motorcycle) v);
        }

        return ms.toArray(new Motorcycle[]{});
    }

    public Tourism[] getTourisms() {
        ArrayList<Tourism> ts = new ArrayList<>();
        for(Vehicle v : vehicles) {
            if(v instanceof Tourism)
                ts.add((Tourism) v);
        }

        return ts.toArray(new Tourism[]{});
    }

    public Bus[] getBusses() {
        ArrayList<Bus> bs = new ArrayList<>();
        for(Vehicle v : vehicles) {
            if(v instanceof Bus)
                bs.add((Bus) v);
        }

        return bs.toArray(new Bus[]{});
    }

}
