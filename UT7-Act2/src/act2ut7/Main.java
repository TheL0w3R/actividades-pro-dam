package act2ut7;

import act2ut7.options.*;
import com.thel0w3r.tlmenuframework.Locale;
import com.thel0w3r.tlmenuframework.MenuBuilder;

/**
 * Created by thel0w3r on 18/5/18.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        new VehicleManager();
        new MenuBuilder()
                .title("Actividad 2 UT7 [Lorenzo Pinna Rodríguez]")
                .addOption(new AddMotorcycle())
                .addOption(new AddTourism())
                .addOption(new AddBus())
                .addOption(new ShowAll())
                .addOption(new ShowMotorcycles())
                .addOption(new ShowTourisms())
                .addOption(new ShowBusses())
                .build()
                .start();
    }

}
