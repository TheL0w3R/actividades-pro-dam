package act2ut7.options;

import act2ut7.InputSystem;
import act2ut7.VehicleManager;
import act2ut7.factories.BusFactory;
import act2ut7.factories.MotorcycleFactory;
import com.thel0w3r.tlmenuframework.MenuOption;

import java.time.LocalDate;

/**
 * Created by TheL0w3R on 22/05/2018.
 * All Rights Reserved.
 */
public class AddBus extends MenuOption {

    public AddBus() {
        super("Add new bus");
    }

    @Override
    public void run() {
        BusFactory bf = new BusFactory();
        bf.setRegNum(InputSystem.readString(sc,"Enter the bus reg. number: "));
        bf.setNumChassis(InputSystem.readInt(sc, "Enter the bus chassis number: "));
        sc.nextLine();
        bf.setBrand(InputSystem.readString(sc, "Enter the bus brand name: "));
        bf.setPower(InputSystem.readDouble(sc, "Enter the bus power: "));
        sc.nextLine();
        bf.setOwnerDNI(InputSystem.readString(sc, "Enter the bus owner DNI: "));
        bf.setNumSeats(InputSystem.readInt(sc, "Enter the bus number of seats: "));
        bf.setService(InputSystem.readService(sc, "Enter the bus service type: "));
        sc.nextLine();
        bf.setRegDate(LocalDate.parse(InputSystem.readString(sc,"Enter the bus reg. date: ")));

        VehicleManager.getInstance().addVehicle(bf.build());
    }
}
