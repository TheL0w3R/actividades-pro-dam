package act2ut7.options;

import act2ut7.VehicleManager;
import act2ut7.vehicles.Bus;
import act2ut7.vehicles.Motorcycle;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 22/5/18.
 * All Rights Reserved.
 */
public class ShowBusses extends MenuOption {

    public ShowBusses() {
        super("Show all busses");
    }

    @Override
    public void run() {
        Bus[] busses = VehicleManager.getInstance().getBusses();
        StringBuilder sb = new StringBuilder();
        System.out.println("Registered busses:");
        if(busses.length > 0) {
            for(Bus b : busses) {
                sb.append(" ").append(b.getRegNum()).append("\n");
                sb.append("  - Chassis number: ").append(b.getNumChassis()).append("\n");
                sb.append("  - Brand: ").append(b.getBrand()).append("\n");
                sb.append("  - Power: ").append(b.getPower()).append("\n");
                sb.append("  - Owner DNI: ").append(b.getOwnerDNI()).append("\n");
                sb.append("  - Registration date: ").append(b.getRegDate()).append("\n");
                sb.append("  - Number of seats: ").append(b.getNumSeats()).append("\n");
                sb.append("  - Service type: ").append(b.getService().getName()).append("\n");
                sb.append("  - This bus must be taken to the ITV at day ").append(b.itv()).append("\n\n");
            }
            System.out.println(sb);
        } else {
            System.out.println("No busses registered yet");
        }
    }
}
