package act2ut7.options;

import act2ut7.InputSystem;
import act2ut7.VehicleManager;
import act2ut7.factories.MotorcycleFactory;
import com.thel0w3r.tlmenuframework.MenuOption;

import java.time.LocalDate;

/**
 * Created by TheL0w3R on 22/05/2018.
 * All Rights Reserved.
 */
public class AddMotorcycle extends MenuOption {

    public AddMotorcycle() {
        super("Add new motorcycle");
    }

    @Override
    public void run() {
        MotorcycleFactory mf = new MotorcycleFactory();
        mf.setRegNum(InputSystem.readString(sc,"Enter the motorcycle's reg. number: "));
        mf.setNumChassis(InputSystem.readInt(sc, "Enter the motorcycle's chassis number: "));
        sc.nextLine();
        mf.setBrand(InputSystem.readString(sc, "Enter the motorcycle's brand name: "));
        mf.setPower(InputSystem.readDouble(sc, "Enter the motorcycle's power: "));
        sc.nextLine();
        mf.setOwnerDNI(InputSystem.readString(sc, "Enter the motorcycle's owner DNI: "));
        mf.setRegDate(LocalDate.parse(InputSystem.readString(sc,"Enter the motorcycle's reg. date: ")));

        VehicleManager.getInstance().addVehicle(mf.build());
    }
}
