package act2ut7.options;

import act2ut7.VehicleManager;
import act2ut7.vehicles.Motorcycle;
import act2ut7.vehicles.Tourism;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 22/5/18.
 * All Rights Reserved.
 */
public class ShowTourisms extends MenuOption {

    public ShowTourisms() {
        super("Show all tourisms");
    }

    @Override
    public void run() {
        Tourism[] tourisms = VehicleManager.getInstance().getTourisms();
        StringBuilder sb = new StringBuilder();
        System.out.println("Registered tourisms:");
        if(tourisms.length > 0) {
            for(Tourism t : tourisms) {
                sb.append(" ").append(t.getRegNum()).append("\n");
                sb.append("  - Chassis number: ").append(t.getNumChassis()).append("\n");
                sb.append("  - Brand: ").append(t.getBrand()).append("\n");
                sb.append("  - Power: ").append(t.getPower()).append("\n");
                sb.append("  - Owner DNI: ").append(t.getOwnerDNI()).append("\n");
                sb.append("  - Registration date: ").append(t.getRegDate()).append("\n");
                sb.append("  - Number of doors: ").append(t.getNumDoors()).append("\n");
                sb.append("  - Number of seats: ").append(t.getNumSeats()).append("\n");
                sb.append("  - ").append(t.isCanTow() ? "The tourism can tow\n" : "The tourism can't tow\n");
                sb.append("  - This tourism must be taken to the ITV at day ").append(t.itv()).append("\n\n");
            }
            System.out.println(sb);
        } else {
            System.out.println("No tourisms registered yet");
        }
    }
}
