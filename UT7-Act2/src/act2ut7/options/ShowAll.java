package act2ut7.options;

import act2ut7.Vehicle;
import act2ut7.VehicleManager;
import act2ut7.vehicles.Bus;
import act2ut7.vehicles.Motorcycle;
import act2ut7.vehicles.Tourism;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 22/5/18.
 * All Rights Reserved.
 */
public class ShowAll extends MenuOption {

    public ShowAll() {
        super("Show all vehicles");
    }

    @Override
    public void run() {
        Vehicle[] vehicles = VehicleManager.getInstance().getAll();
        StringBuilder sb = new StringBuilder();
        System.out.println("Registered vehicles:");
        if(vehicles.length > 0) {
            for (Vehicle v : vehicles) {
                sb.append(" ").append(v.getRegNum()).append(" [").append(getType(v)).append("]\n");
                sb.append("  - Chassis number: ").append(v.getNumChassis()).append("\n");
                sb.append("  - Brand: ").append(v.getBrand()).append("\n");
                sb.append("  - Power: ").append(v.getPower()).append("\n");
                sb.append("  - Owner DNI: ").append(v.getOwnerDNI()).append("\n");
                sb.append("  - Registration date: ").append(v.getRegDate()).append("\n");
                if(v instanceof Tourism) {
                    Tourism t = (Tourism)v;
                    sb.append("  - Number of doors: ").append(t.getNumDoors()).append("\n");
                    sb.append("  - Number of seats: ").append(t.getNumSeats());
                    sb.append("  - ").append(t.isCanTow() ? "The tourism can tow\n" : "The tourism can't tow\n");
                } else if(v instanceof Bus) {
                    Bus b = (Bus)v;
                    sb.append("  - Number of seats: ").append(b.getNumSeats()).append("\n");
                    sb.append("  - Service type: ").append(b.getService().getName()).append("\n");
                }

                sb.append("  - This vehicle must be taken to the ITV at day ").append(v.itv()).append("\n\n");
            }
            System.out.println(sb);
        } else {
            System.out.println("No vehicles registered yet");
        }
    }

    private String getType(Vehicle v) {
        String type = "";
        if(v instanceof Motorcycle)
            type = "Motorcycle";
        else if (v instanceof Tourism)
            type = "Tourism";
        else
            type = "Bus";

        return type;
    }
}
