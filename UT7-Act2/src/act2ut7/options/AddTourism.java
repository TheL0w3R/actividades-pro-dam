package act2ut7.options;

import act2ut7.InputSystem;
import act2ut7.VehicleManager;
import act2ut7.factories.MotorcycleFactory;
import act2ut7.factories.TourismFactory;
import com.thel0w3r.tlmenuframework.MenuOption;

import java.time.LocalDate;

/**
 * Created by TheL0w3R on 22/05/2018.
 * All Rights Reserved.
 */
public class AddTourism extends MenuOption {

    public AddTourism() {
        super("Add new tourism");
    }

    @Override
    public void run() {
        TourismFactory tf = new TourismFactory();
        tf.setRegNum(InputSystem.readString(sc,"Enter the tourism's reg. number: "));
        tf.setNumChassis(InputSystem.readInt(sc, "Enter the tourism's chassis number: "));
        sc.nextLine();
        tf.setBrand(InputSystem.readString(sc, "Enter the tourism's brand name: "));
        tf.setPower(InputSystem.readDouble(sc, "Enter the tourism's power: "));
        sc.nextLine();
        tf.setOwnerDNI(InputSystem.readString(sc, "Enter the tourism's owner DNI: "));
        tf.setNumDoors(InputSystem.readInt(sc, "Enter the tourism's number of doors: "));
        tf.setNumSeats(InputSystem.readInt(sc, "Enter the tourism's number of seats: "));
        tf.setCanTow(InputSystem.readBoolean(sc, "Enter the ability of the tourism to tow: "));
        sc.nextLine();
        tf.setRegDate(LocalDate.parse(InputSystem.readString(sc,"Enter the tourism's reg. date: ")));

        VehicleManager.getInstance().addVehicle(tf.build());
    }
}
