package act2ut7.options;

import act2ut7.VehicleManager;
import act2ut7.vehicles.Motorcycle;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 22/5/18.
 * All Rights Reserved.
 */
public class ShowMotorcycles extends MenuOption {

    public ShowMotorcycles() {
        super("Show all motorcycles");
    }

    @Override
    public void run() {
        Motorcycle[] motorcycles = VehicleManager.getInstance().getMotorcycles();
        StringBuilder sb = new StringBuilder();
        System.out.println("Registered motorcycles:");
        if(motorcycles.length > 0) {
            for(Motorcycle m : motorcycles) {
                sb.append(" ").append(m.getRegNum()).append("\n");
                sb.append("  - Chassis number: ").append(m.getNumChassis()).append("\n");
                sb.append("  - Brand: ").append(m.getBrand()).append("\n");
                sb.append("  - Power: ").append(m.getPower()).append("\n");
                sb.append("  - Owner DNI: ").append(m.getOwnerDNI()).append("\n");
                sb.append("  - Registration date: ").append(m.getRegDate()).append("\n");
                sb.append("  - This motorcycle must be taken to the ITV at day ").append(m.itv()).append("\n\n");
            }
            System.out.println(sb);
        } else {
            System.out.println("No motorcycles registered yet");
        }
    }
}
