package act2ut7;

/**
 * Created by thel0w3r on 22/5/18.
 * All Rights Reserved.
 */
public enum ServiceType {
    PUBLIC("Public"), SCHOOL("School"), DISCRETE("Discrete");

    private String name;

    ServiceType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
