package act2ut7.vehicles;

import act2ut7.Vehicle;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 19/5/18.
 * All Rights Reserved.
 */
public class Tourism extends Vehicle {

    private int numDoors;
    private boolean canTow;
    private int numSeats;

    public Tourism(String regNum, int numChassis, String brand, double power, String ownerDNI, LocalDate regDate, int numDoors, boolean canTow, int numSeats) {
        super(regNum, numChassis, brand, power, ownerDNI, regDate);
        this.numDoors = numDoors;
        this.canTow = canTow;
        this.numSeats = numSeats;
    }

    @Override
    public LocalDate itv() {
        return regDate.plusYears(5);
    }

    public int getNumDoors() {
        return numDoors;
    }

    public void setNumDoors(int numDoors) {
        this.numDoors = numDoors;
    }

    public boolean isCanTow() {
        return canTow;
    }

    public void setCanTow(boolean canTow) {
        this.canTow = canTow;
    }

    public int getNumSeats() {
        return numSeats;
    }

    public void setNumSeats(int numSeats) {
        this.numSeats = numSeats;
    }
}
