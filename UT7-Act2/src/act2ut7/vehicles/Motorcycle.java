package act2ut7.vehicles;

import act2ut7.Vehicle;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 19/5/18.
 * All Rights Reserved.
 */
public class Motorcycle extends Vehicle {

    public Motorcycle(String regNum, int numChassis, String brand, double power, String ownerDNI, LocalDate regDate) {
        super(regNum, numChassis, brand, power, ownerDNI, regDate);
    }

    @Override
    public LocalDate itv() {
        return regDate.plusYears(5);
    }
}
