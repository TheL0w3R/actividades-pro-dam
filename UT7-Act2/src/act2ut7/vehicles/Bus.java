package act2ut7.vehicles;

import act2ut7.ServiceType;
import act2ut7.Vehicle;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 22/5/18.
 * All Rights Reserved.
 */
public class Bus extends Vehicle {

    private int numSeats;
    private ServiceType service;

    public Bus(String regNum, int numChassis, String brand, double power, String ownerDNI, LocalDate regDate, int numSeats, ServiceType service) {
        super(regNum, numChassis, brand, power, ownerDNI, regDate);
        this.numSeats = numSeats;
        this.service = service;
    }

    @Override
    public LocalDate itv() {
        return regDate.plusYears(2);
    }

    public int getNumSeats() {
        return numSeats;
    }

    public void setNumSeats(int numSeats) {
        this.numSeats = numSeats;
    }

    public ServiceType getService() {
        return service;
    }

    public void setService(ServiceType service) {
        this.service = service;
    }
}
