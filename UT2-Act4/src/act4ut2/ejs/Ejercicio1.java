package act4ut2.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by TheL0w3R on 08/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio1 extends MenuOption {

    public Ejercicio1() {
        super("Ejercicio 1");
    }

    @Override
    public void run() {
        int countA = 0, countB = 0, countC = 0, countX = 0;
        boolean exit = false;
        System.out.println("  - A (Categoría A)\n  - B (Categoría B)\n  - C (Categoría C)" +
                "\n  - X (Sin estudio energético)\n  - # (SALIR)");
        while(!exit) {
            System.out.print("Introduce una categoría: ");
            char cat = Character.toUpperCase(sc.next().charAt(0));
            switch (cat) {
                case 'A':
                    countA++;
                    break;
                case 'B':
                    countB++;
                    break;
                case 'C':
                    countC++;
                    break;
                case 'X':
                    countX++;
                    break;
                case '#':
                    exit = true;
                    break;
                default:
                    System.out.println("No es una opción válida.");
                    break;
            }
        }
        double tot = countA+countB+countC+countX;
        System.out.println("Has visitado un total de " + (int)tot + " casas.");
        System.out.println(countA + " casas (" + getPercentage(tot, countA) + "%) cumplen con la categoría A.");
        System.out.println(countB + " casas (" + getPercentage(tot, countB) + "%) cumplen con la categoría B.");
        System.out.println(countC + " casas (" + getPercentage(tot, countC) + "%) cumplen con la categoría C.");
        System.out.println(countX + " casas (" + getPercentage(tot, countX) + "%) no tienen estudio energético.");
    }

    private double getPercentage(double tot, double n) {
        return (double)Math.round(((n / tot) * 100) * 100) / 100;
    }
}
