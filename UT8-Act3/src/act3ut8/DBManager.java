package act3ut8;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by thel0w3r on 27/5/18.
 * All Rights Reserved.
 */
public class DBManager {

    private static DBManager instance;

    private Connection conn;

    public DBManager() {
        instance = this;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ACT3UT8", "root", "");
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static DBManager getInstance() {
        return instance;
    }

    public Song[] getByGenere(String genere) {
        ArrayList<Song> songs = new ArrayList<>();
        try {
            PreparedStatement ps = this.conn.prepareStatement("SELECT * FROM canciones WHERE (canciones.genere LIKE '" + genere + "')");
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                songs.add(new Song(rs.getString("name"),
                        rs.getString("author"),
                        rs.getString("album"),
                        rs.getString("genere"),
                        rs.getInt("year")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return songs.toArray(new Song[]{});
    }

    public String[] getAllGeneres() {
        ArrayList<String> generes = new ArrayList<>();
        try {
            PreparedStatement ps = this.conn.prepareStatement("SELECT DISTINCT(genere) FROM canciones");
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                generes.add(rs.getString("genere"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return generes.toArray(new String[]{});
    }

}
