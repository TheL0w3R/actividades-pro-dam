package act3ut8;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created by thel0w3r on 27/5/18.
 * All Rights Reserved.
 */
public class FileManager {

    private static FileManager instance;

    private File songListPath;

    public FileManager() {
        instance = this;
        songListPath = new File(System.getProperty("user.dir").replace("\\", "/") + "/songlists/");
        if(!songListPath.exists())
            songListPath.mkdir();
    }

    public static FileManager getInstance() {
        return instance;
    }

    public void dumpSongs(String fileName, Song[] songs) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(songListPath + "/" + fileName + ".songlist"));
            oos.writeObject(new ArrayList<>(Arrays.asList(songs)));
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public File[] getSongLists() {
        ArrayList<File> files = new ArrayList<>();
        for(File f : Objects.requireNonNull(songListPath.listFiles())) {
            if(f.isFile() && f.getName().endsWith(".songlist"))
                files.add(f);
        }
        return files.toArray(new File[]{});
    }

    public Song[] readSongList(File f) {
        ArrayList<Song> deserialized = null;
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
            deserialized = (ArrayList<Song>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return deserialized.toArray(new Song[]{});
    }

}
