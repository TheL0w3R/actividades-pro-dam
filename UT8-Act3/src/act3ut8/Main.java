package act3ut8;

import act3ut8.options.DumpSongList;
import act3ut8.options.ReadSongList;
import com.thel0w3r.tlmenuframework.MenuBuilder;

/**
 * Created by thel0w3r on 27/5/18.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        new FileManager();
        new DBManager();
        new MenuBuilder()
                .title("Actividad 3 UT8 [Lorenzo Pinna Rodríguez]")
                .addOption(new DumpSongList())
                .addOption(new ReadSongList())
                .build()
                .start();
    }

}
