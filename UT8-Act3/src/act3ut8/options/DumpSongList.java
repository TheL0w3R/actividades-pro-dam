package act3ut8.options;

import act3ut8.DBManager;
import act3ut8.FileManager;
import act3ut8.InputSystem;
import com.thel0w3r.tlmenuframework.MenuOption;

import java.io.File;
import java.util.InputMismatchException;

/**
 * Created by thel0w3r on 27/5/18.
 * All Rights Reserved.
 */
public class DumpSongList extends MenuOption {

    public DumpSongList() {
        super("Dump song list");
    }

    @Override
    public void run() {
        String[] generes = DBManager.getInstance().getAllGeneres();
        StringBuilder sb = new StringBuilder("Currently available generes\n");

        for(int i = 0; i < generes.length; i++)
            sb.append("  ").append(i+1).append(" - ").append(generes[i]).append("\n");

        sb.append("  0 - Cancel\n").append("\nSelect a genere to dump all matching songs to a file: ");

        int op = InputSystem.readInt(sc, sb.toString());
        sc.nextLine();
        if(op < 0 || op > generes.length)
            System.out.println("This genere doesn't exist.");
        else if(op != 0)
            FileManager.getInstance().dumpSongs(generes[op - 1], DBManager.getInstance().getByGenere(generes[op - 1]));
    }
}
