package act3ut8.options;

import act3ut8.FileManager;
import act3ut8.InputSystem;
import act3ut8.Song;
import com.thel0w3r.tlmenuframework.MenuOption;

import java.io.File;

/**
 * Created by thel0w3r on 27/5/18.
 * All Rights Reserved.
 */
public class ReadSongList extends MenuOption {

    public ReadSongList() {
        super("Read song List");
    }

    @Override
    public void run() {
        File[] songLists = FileManager.getInstance().getSongLists();
        StringBuilder sb = new StringBuilder("Currently available song lists\n");

        for(int i = 0; i < songLists.length; i++)
            sb.append("  ").append(i+1).append(" - ").append(songLists[i].getName()
                    .substring(0, songLists[i].getName().indexOf("."))).append("\n");

        sb.append("  0 - Cancel\n").append("\nSelect a list to view it's content: ");

        int op = InputSystem.readInt(sc, sb.toString());
        sc.nextLine();
        if(op < 0 || op > songLists.length)
            System.out.println("This genere doesn't exist.");
        else if(op != 0) {
            Song[] songs = FileManager.getInstance().readSongList(songLists[op - 1]);
            for(Song s : songs)
                System.out.println("  " + s.getAuthor() + " - " + s.getName());
        }
    }
}
