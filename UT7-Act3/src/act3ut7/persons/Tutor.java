package act3ut7.persons;

import java.time.LocalDate;

/**
 * Created by TheL0w3R on 25/05/2018.
 * All Rights Reserved.
 */
public class Tutor extends Teacher {

    private String group;

    public Tutor(String name, String dni, LocalDate birthDate, String nrp, int spec, String college, double salary, String group) {
        super(name, dni, birthDate, nrp, spec, college, salary);
        this.group = group;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public double getSalary() {
        double plus = this.salary * COMPLEMENT;
        return this.salary + plus;
    }
}
