package act3ut7.persons;

import act3ut7.interfaces.IAccess;
import act3ut7.Person;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class Student extends Person implements IAccess {

    private String cial;
    private String course;

    public Student(String name, String dni, LocalDate birthDate, String cial, String course) {
        super(name, dni, birthDate);
        this.cial = cial;
        this.course = course;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    @Override
    public void setId(String id) {
        this.cial = id;
    }

    @Override
    public String getId() {
        return this.cial;
    }
}
