package act3ut7.persons;

import act3ut7.interfaces.IAccess;
import act3ut7.interfaces.ISalary;
import act3ut7.Person;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class Teacher extends Person implements IAccess, ISalary {

    private String nrp;
    private int spec;
    private String college;
    protected double salary;

    public Teacher(String name, String dni, LocalDate birthDate, String nrp, int spec, String college, double salary) {
        super(name, dni, birthDate);
        this.nrp = nrp;
        this.spec = spec;
        this.college = college;
        this.salary = salary;
    }

    public int getSpec() {
        return spec;
    }

    public void setSpec(int spec) {
        this.spec = spec;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public void setId(String id) {
        this.nrp = id;
    }

    @Override
    public String getId() {
        return nrp;
    }

    @Override
    public double getSalary() {
        return salary;
    }
}
