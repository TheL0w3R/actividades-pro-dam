package act3ut7.options;

import act3ut7.PersonManager;
import act3ut7.persons.Teacher;
import act3ut7.persons.Tutor;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class ShowTutors extends MenuOption {

    public ShowTutors() {
        super("Show Tutors");
    }

    @Override
    public void run() {
        PersonManager pm = PersonManager.getInstance();
        StringBuilder sb = new StringBuilder("Registered teachers:\n\n");
        for (Tutor t : pm.getTutors()) {
            sb.append(" ").append(t.getName()).append("\n");
            sb.append("  - DNI: ").append(t.getDni()).append("\n");
            sb.append("  - Birth Date: ").append(t.getBirthDate()).append("\n");
            sb.append("  - Age: ").append(t.age()).append("\n");
            sb.append("  - NRP: ").append(t.getId()).append("\n");
            sb.append("  - College: ").append(t.getCollege()).append("\n");
            sb.append("  - Speciality: ").append(t.getSpec()).append("\n");
            sb.append("  - Salary: ").append(t.getSalary()).append("\n");
            sb.append("  - Group: ").append(t.getGroup()).append("\n");
        }

        System.out.print(sb);
    }
}
