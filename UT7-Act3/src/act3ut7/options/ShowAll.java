package act3ut7.options;

import act3ut7.Person;
import act3ut7.PersonManager;
import act3ut7.persons.Student;
import act3ut7.persons.Teacher;
import act3ut7.persons.Tutor;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by TheL0w3R on 25/05/2018.
 * All Rights Reserved.
 */
public class ShowAll extends MenuOption {

    public ShowAll() {
        super("Show all persons");
    }

    @Override
    public void run() {
        PersonManager pm = PersonManager.getInstance();
        StringBuilder sb = new StringBuilder("Registered persons:\n\n");
        for (Person p : pm.getPersons()) {
            sb.append(" ").append(p.getName()).append(" (").append(getPersonType(p)).append(")\n");
            sb.append("  - DNI: ").append(p.getDni()).append("\n");
            sb.append("  - Birth Date: ").append(p.getBirthDate()).append("\n");
            sb.append("  - Age: ").append(p.age()).append("\n");
            if(p instanceof Teacher) {
                Teacher t = (Teacher)p;
                sb.append("  - NRP: ").append(t.getId()).append("\n");
                sb.append("  - College: ").append(t.getCollege()).append("\n");
                sb.append("  - Speciality: ").append(t.getSpec()).append("\n");
                sb.append("  - Salary: ").append(t.getSalary()).append("\n");
                if(t instanceof Tutor)
                    sb.append("  - Group: ").append(((Tutor)t).getGroup()).append("\n");
            } else if(p instanceof Student) {
                Student s = (Student)p;
                sb.append("  - CIAL: ").append(s.getId()).append("\n");
                sb.append("  - Course: ").append(s.getCourse()).append("\n");
            }
            sb.append("\n");
        }

        System.out.print(sb);
    }

    private String getPersonType(Person p) {
        String type = "";
        if(p instanceof Student)
            type = "Student";
        else if (p instanceof Teacher) {
            type = "Teacher";
            Teacher t = (Teacher)p;
            if(t instanceof Tutor)
                type = "Tutor";
        }
        return type;
    }
}
