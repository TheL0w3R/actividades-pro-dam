package act3ut7.options;

import act3ut7.InputSystem;
import act3ut7.PersonManager;
import act3ut7.factories.TeacherFactory;
import act3ut7.factories.TutorFactory;
import com.thel0w3r.tlmenuframework.MenuOption;

import java.time.LocalDate;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class AddTutor extends MenuOption {

    public AddTutor() {
        super("Add a new Tutor");
    }

    @Override
    public void run() {
        TutorFactory tf = new TutorFactory();
        tf.setDni(InputSystem.readString(sc, "Enter the tutor DNI: "));
        tf.setName(InputSystem.readString(sc, "Enter the tutor name: "));
        tf.setBirthDate(LocalDate.of(
                InputSystem.readInt(sc, "Enter the tutor birth year: "),
                InputSystem.readInt(sc, "Enter the tutor birth month: "),
                InputSystem.readInt(sc, "Enter the tutor birth day: ")
        ));
        sc.nextLine();
        tf.setNrp(InputSystem.readString(sc, "Enter the tutor NRP: "));
        tf.setSpec(InputSystem.readInt(sc, "Enter the tutor speciality ID: "));
        sc.nextLine();
        tf.setCollege(InputSystem.readString(sc, "Enter the tutor college: "));
        tf.setSalary(InputSystem.readDouble(sc, "Enter the tutor raw salary: "));
        tf.setGroup(InputSystem.readString(sc, "Enter the tutor group: "));

        PersonManager.getInstance().addPerson(tf.build());

        System.out.println("\nTutor added successfully!");
    }

}
