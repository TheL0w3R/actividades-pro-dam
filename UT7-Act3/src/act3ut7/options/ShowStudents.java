package act3ut7.options;

import act3ut7.PersonManager;
import act3ut7.persons.Student;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class ShowStudents extends MenuOption {

    public ShowStudents() {
        super("Show Students");
    }

    @Override
    public void run() {
        PersonManager pm = PersonManager.getInstance();
        StringBuilder sb = new StringBuilder("Registered students:\n\n");
        for (Student s : pm.getStudents()) {
            sb.append(" ").append(s.getName()).append("\n");
            sb.append("  - DNI: ").append(s.getDni()).append("\n");
            sb.append("  - Birth Date: ").append(s.getBirthDate()).append("\n");
            sb.append("  - Age: ").append(s.age()).append("\n");
            sb.append("  - CIAL: ").append(s.getId()).append("\n");
            sb.append("  - Course: ").append(s.getCourse()).append("\n");
        }

        System.out.print(sb);
    }
}
