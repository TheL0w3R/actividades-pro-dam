package act3ut7.factories;

import act3ut7.persons.Tutor;

import java.time.LocalDate;

/**
 * Created by TheL0w3R on 25/05/2018.
 * All Rights Reserved.
 */
public class TutorFactory {

    private String name;
    private String dni;
    private LocalDate birthDate;
    private String nrp;
    private int spec;
    private String college;
    private double salary;
    private String group;

    public TutorFactory setName(String name) {
        this.name = name;
        return this;
    }

    public TutorFactory setDni(String dni) {
        this.dni = dni;
        return this;
    }

    public TutorFactory setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public TutorFactory setNrp(String nrp) {
        this.nrp = nrp;
        return this;
    }

    public TutorFactory setSpec(int spec) {
        this.spec = spec;
        return this;
    }

    public TutorFactory setCollege(String college) {
        this.college = college;
        return this;
    }

    public TutorFactory setSalary(double salary) {
        this.salary = salary;
        return this;
    }

    public TutorFactory setGroup(String group) {
        this.group = group;
        return this;
    }

    public Tutor build() {
        return new Tutor(dni, name, birthDate, nrp, spec, college, salary, group);
    }
}
