package act3ut7.factories;

import act3ut7.persons.Teacher;

import java.time.LocalDate;

/**
 * Created by TheL0w3R on 25/05/2018.
 * All Rights Reserved.
 */
public class TeacherFactory {

    private String name;
    private String dni;
    private LocalDate birthDate;
    private String nrp;
    private int spec;
    private String college;
    private double salary;

    public TeacherFactory setName(String name) {
        this.name = name;
        return this;
    }

    public TeacherFactory setDni(String dni) {
        this.dni = dni;
        return this;
    }

    public TeacherFactory setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public TeacherFactory setNrp(String nrp) {
        this.nrp = nrp;
        return this;
    }

    public TeacherFactory setSpec(int spec) {
        this.spec = spec;
        return this;
    }

    public TeacherFactory setCollege(String college) {
        this.college = college;
        return this;
    }

    public TeacherFactory setSalary(double salary) {
        this.salary = salary;
        return this;
    }

    public Teacher build() {
        return new Teacher(name, dni, birthDate, nrp, spec, college, salary);
    }
}
