package act3ut7.factories;

import act3ut7.persons.Student;

import java.time.LocalDate;

/**
 * Created by TheL0w3R on 25/05/2018.
 * All Rights Reserved.
 */
public class StudentFactory {

    private String name;
    private String dni;
    private LocalDate birthDate;
    private String cial;
    private String course;

    public StudentFactory setName(String name) {
        this.name = name;
        return this;
    }

    public StudentFactory setDni(String dni) {
        this.dni = dni;
        return this;
    }

    public StudentFactory setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public StudentFactory setCial(String cial) {
        this.cial = cial;
        return this;
    }

    public StudentFactory setCourse(String course) {
        this.course = course;
        return this;
    }

    public Student build() {
        return new Student(name, dni, birthDate, cial, course);
    }
}
