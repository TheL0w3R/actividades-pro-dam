package act3ut7;

import act3ut7.persons.Student;
import act3ut7.persons.Teacher;
import act3ut7.persons.Tutor;

import java.util.ArrayList;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class PersonManager {

    private static PersonManager instance;

    private ArrayList<Person> persons = new ArrayList<>();

    public PersonManager() {
        instance = this;
    }

    public void addPerson(Person p) {
        if(!persons.contains(p))
            persons.add(p);
    }

    public void removePerson(String name) {
        persons.removeIf(s -> s.getName().equalsIgnoreCase(name));
    }

    public Person[] getPersons() {
        return persons.toArray(new Person[]{});
    }

    public Teacher[] getTeachers() {
        ArrayList<Teacher> teachers = new ArrayList<>();
        for(Person p : persons) {
            if(p instanceof Teacher)
                teachers.add((Teacher)p);
        }
        return teachers.toArray(new Teacher[]{});
    }

    public Tutor[] getTutors() {
        ArrayList<Tutor> tutors = new ArrayList<>();
        for(Person p : persons) {
            if(p instanceof Tutor)
                tutors.add((Tutor)p);
        }
        return tutors.toArray(new Tutor[]{});
    }

    public Student[] getStudents() {
        ArrayList<Student> teachers = new ArrayList<>();
        for(Person p : persons) {
            if(p instanceof Student)
                teachers.add((Student) p);
        }
        return teachers.toArray(new Student[]{});
    }

    public static PersonManager getInstance() {
        return instance;
    }
}
