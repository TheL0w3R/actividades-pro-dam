package act3ut7;

import act3ut7.options.*;
import act3ut7.persons.Tutor;
import com.thel0w3r.tlmenuframework.MenuBuilder;

/**
 * Created by TheL0w3R on 25/05/2018.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        new PersonManager();
        new MenuBuilder()
                .title("Actividad 3 UT7 [Lorenzo Pinna Rodríguez]")
                .addOption(new AddStudent())
                .addOption(new AddTeacher())
                .addOption(new AddTutor())
                .addOption(new ShowStudents())
                .addOption(new ShowTeachers())
                .addOption(new ShowTutors())
                .addOption(new ShowAll())
                .build()
                .start();
    }

}
