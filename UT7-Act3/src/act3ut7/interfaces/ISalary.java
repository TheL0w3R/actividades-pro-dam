package act3ut7.interfaces;

/**
 * Created by TheL0w3R on 25/05/2018.
 * All Rights Reserved.
 */
public interface ISalary {

    double COMPLEMENT = 0.10D;

    double getSalary();

}
