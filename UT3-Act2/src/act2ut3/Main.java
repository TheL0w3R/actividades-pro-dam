package act2ut3;

import act2ut3.ejs.Ejercicio1;
import com.thel0w3r.tlmenuframework.Locale;
import com.thel0w3r.tlmenuframework.Menu;
import com.thel0w3r.tlmenuframework.MenuBuilder;

/**
 * Created by TheL0w3R on 21/11/2017.
 * All Rights Reserved.
 */
public class Main {
    public static void main(String[] args) {
        Menu m = new MenuBuilder()
                .title("Actividad 2 UT3 [Lorenzo Pinna Rodríguez]")
                .setLocale(Locale.ES)
                .addOption(new Ejercicio1())
                .build();
        m.start();
    }
}
