package act2ut3.ejs;

import com.thel0w3r.tlmenuframework.MenuOption;

import java.util.InputMismatchException;

/**
 * Created by TheL0w3R on 21/11/2017.
 * All Rights Reserved.
 */
public class Ejercicio1 extends MenuOption {

    private final double BAJO = 18.5;
    private final double NORMAL = 25;
    private final double SOBREPESO = 30;
    private final double OB_LEVE = 35;
    private final double OB_MEDIA = 40;

    public Ejercicio1() {
        super("Ejercicio 1 (IMC)");
    }

    @Override
    public void run() {
        double weight = 0;
        double height = 0;

        do {
            try {
                System.out.print("Introduce tu peso (KG): ");
                weight = sc.nextDouble();
                if(weight < 0) {
                    System.out.println("No puedes introducir un peso negativo.");
                } else {
                    System.out.print("Introduce tu altura (M): ");
                    height = sc.nextDouble();
                    if(height < 0) {
                        System.out.println("No puedes introducir una altura negativa.");
                    } else
                        break;
                }
            } catch (InputMismatchException e) {
                System.out.println("El valor que has introducido no es válido.");
                sc.nextLine();
            }
        } while(true);

        double imc = getIMC(weight, height);

        System.out.println("Tu IMC es: " + imc);

        if(imc < BAJO && imc >= 0)
            System.out.println("Tienes un peso bajo.");
        else if(imc < NORMAL && imc >= BAJO)
            System.out.println("Tienes un peso normal.");
        else if(imc < SOBREPESO && imc >= NORMAL)
            System.out.println("Tienes sobrepeso.");
        else if(imc < OB_LEVE && imc >= SOBREPESO)
            System.out.println("Tienes obesidad leve.");
        else if(imc < OB_MEDIA && imc >= OB_LEVE)
            System.out.println("Tienes obesidad media.");
        else if(imc > OB_MEDIA)
            System.out.println("Tienes obesidad mórbida.");
    }

    private double getIMC(double w, double h) {
        return (double)Math.round((w/(Math.pow(h, 2))) * 100) / 100;
    }
}
