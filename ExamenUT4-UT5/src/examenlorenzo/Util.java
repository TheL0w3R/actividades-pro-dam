package examenlorenzo;

/**
 * Created by thel0w3r on 9/3/18.
 * All Rights Reserved.
 */
public class Util {

    public static void printTable(String[][] data, String title) {
        StringBuilder sb = new StringBuilder("\n");
        int[] rowSpaces = new int[data[0].length];
        for(String[] row : data) {
            for(int i = 0; i < row.length; i++) {
                if(row[i].length() > (rowSpaces[i] - 4)) {
                    rowSpaces[i] = row[i].length() + 4;
                }
            }
        }

        int totalSpaces = 2;

        for(int spacer : rowSpaces) {
            totalSpaces += spacer;
        }
        int titleSpacer = totalSpaces - title.length();
        sb.append(getStr(titleSpacer/2, " ")).append(title);
        if(titleSpacer % 2 == 0)
            sb.append(getStr(titleSpacer/2, " "));
        else
            sb.append(getStr((titleSpacer/2) + 1, " "));
        sb.append("\n").append(getStr(totalSpaces, "=")).append("\n");
        for(String[] row : data) {
            sb.append("=");
            for(int i = 0; i < row.length; i++) {
                int currentSpacing = rowSpaces[i] - row[i].length();
                String spacer = getStr(currentSpacing/2, " ");
                if(currentSpacing % 2 == 0) {
                    //SPACING IS PAIR
                    sb.append(spacer).append(row[i]).append(spacer);
                } else {
                    //SPACING IS ODD
                    sb.append(spacer).append(row[i]).append(getStr((currentSpacing/2) + 1, " "));
                }
            }
            sb.append("=\n");
        }
        sb.append(getStr(totalSpaces, "=")).append("\n");
        System.out.println(sb);
    }

    private static String getStr(int l, String c) {
        return (new String(new char[l])).replace("\u0000", c);
    }

}
