package examenlorenzo;

/**
 * Created by thel0w3r on 9/3/18.
 * All Rights Reserved.
 */
public class Student {

    private String cial;
    private String name;
    private String course;
    private double mark;

    public Student(String cial, String name, String course, double mark) {
        this.cial = cial;
        this.name = name;
        this.course = course;
        this.mark = mark;
    }

    public Student(String cial, String name) {
        this.cial = cial;
        this.name = name;
    }

    public Student() {
    }

    public String getCial() {
        return cial;
    }

    public void setCial(String cial) {
        this.cial = cial;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public double getMark() {
        return mark;
    }

    public void setMark(double mark) {
        this.mark = mark;
    }
}
