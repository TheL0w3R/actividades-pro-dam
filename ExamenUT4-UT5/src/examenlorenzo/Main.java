package examenlorenzo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by thel0w3r on 9/3/18.
 * All Rights Reserved.
 */
public class Main {

    private String data;
    private Student[] students;
    private String[][] markMatrix;
    private ArrayList<String> biggestMed;

    public Main() {
        this.data = "A25,Alicia,DAM,7.5#A26,Juan Carlos, DAM,5.25#A27,Beatriz, DAW,6.0#A14,Luis, DAW,5.0";
        biggestMed = new ArrayList<>();
    }

    public static void main(String[] args) {
        new Main().init();
    }

    private void init() {
        students = new Student[countStudents()];
        markMatrix = new String[2][4];

        loadStudents();
        showStudents();

        initMatrix();
        fillMatrix();

        Util.printTable(markMatrix, "Medias por módulo");

        fillBiggestMed();

        System.out.println("Ciclo con mayor media: " + getBiggestMedCourse());
        System.out.println("Alumnos pertenecientes al mismo:");
        for(String name : biggestMed) {
            System.out.println(" - " + name);
        }
    }

    private int countStudents() {
        return data.split("#").length;
    }

    private void loadStudents() {
        for(int i = 0; i < students.length; i++) {
            String[] studentData = data.split("#")[i].split(",");
            students[i] = new Student(
                    studentData[0].trim(),
                    studentData[1].trim(),
                    studentData[2].trim(),
                    Double.valueOf(studentData[3].trim())
            );
        }
    }

    private void showStudents() {
        for(Student s : students) {
            System.out.println("\nCIAL: " + s.getCial());
            System.out.println("Nombre: " + s.getName());
            System.out.println("Ciclo: " + s.getCourse());
            System.out.println("Nota: " + s.getMark());
        }
    }

    private void initMatrix() {
        markMatrix[0][0] = "ASIR";
        markMatrix[0][1] = "DAM";
        markMatrix[0][2] = "DAW";
        markMatrix[0][3] = "SMR";
    }

    private void fillMatrix() {
        for(int i = 0; i < markMatrix[0].length; i++) {
            markMatrix[1][i] = String.valueOf(getMedFor(markMatrix[0][i]));
        }
    }

    private double getMedFor(String course) {
        double sum = 0D;
        int am = 0;
        for(Student s : students) {
            if(s.getCourse().equalsIgnoreCase(course.trim())) {
                sum += s.getMark();
                am++;
            }
        }
        return (am > 0) ? round(sum/am) : 0;
    }

    private double round(double d) {
        return Math.round(d * 10D) / 10D;
    }

    private void fillBiggestMed() {
        biggestMed.addAll(Arrays.asList(getStudents(getBiggestMedCourse())));
        Collections.sort(biggestMed);
    }

    private String getBiggestMedCourse() {
        double biggest = 0D;
        int index = 0;
        for(int i = 0; i < markMatrix[0].length; i++) {
            double mark = Double.valueOf(markMatrix[1][i]);
            if(mark > biggest) {
                biggest = mark;
                index = i;
            }
        }

        return markMatrix[0][index];
    }

    private String[] getStudents(String course) {
        ArrayList<String> tmp = new ArrayList<>();
        for(Student s : students) {
            if(s.getCourse().equalsIgnoreCase(course.trim()))
                tmp.add(s.getName());
        }
        return tmp.toArray(new String[tmp.size()]);
    }

}
