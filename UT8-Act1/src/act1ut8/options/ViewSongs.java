package act1ut8.options;

import act1ut8.FileManager;
import act1ut8.Song;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class ViewSongs extends MenuOption {

    public ViewSongs() {
        super("View Songs");
    }

    @Override
    public void run() {
        StringBuilder sb = new StringBuilder("Current songs:\n");
        Song[] songs = FileManager.getInstance().readSongs();
        for(int i = 0; i < songs.length; i++) {
            sb.append("  ").append(i+1).append(" ")
                    .append(songs[i].getAuthor()).append(" - ")
                    .append(songs[i].getName()).append("\n");
        }

        System.out.print(sb);
    }
}
