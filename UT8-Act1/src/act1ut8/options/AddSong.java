package act1ut8.options;

import act1ut8.FileManager;
import act1ut8.InputSystem;
import act1ut8.factories.SongFactory;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class AddSong extends MenuOption {

    public AddSong() {
        super("Add new song");
    }

    @Override
    public void run() {
        SongFactory sf = new SongFactory();
        sf.setName(InputSystem.readString(sc, "Enter the song name: "));
        sf.setAuthor(InputSystem.readString(sc, "Enter the song author: "));
        sf.setAlbum(InputSystem.readString(sc, "Enter the song album: "));
        sf.setGenere(InputSystem.readString(sc, "Enter the song genere: "));
        sf.setYear(InputSystem.readInt(sc, "Enter the song year: "));
        sc.nextLine();
        FileManager.getInstance().writeSong(sf.build());
    }
}
