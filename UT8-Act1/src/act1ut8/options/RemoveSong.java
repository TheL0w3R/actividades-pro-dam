package act1ut8.options;

import act1ut8.FileManager;
import act1ut8.InputSystem;
import act1ut8.Song;
import com.thel0w3r.tlmenuframework.MenuOption;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class RemoveSong extends MenuOption {

    public RemoveSong() {
        super("Remove a song");
    }

    @Override
    public void run() {
        StringBuilder sb = new StringBuilder("Current songs:\n");
        Song[] songs = FileManager.getInstance().readSongs();
        for(int i = 0; i < songs.length; i++) {
            sb.append("  ").append(i+1).append(" ")
                    .append(songs[i].getAuthor()).append(" - ")
                    .append(songs[i].getName()).append("\n");
        }
        sb.append("  0 Cancel\n\nSelect a song to remove: ");
        int op = InputSystem.readInt(sc, sb.toString());
        while(op < 0 || op > songs.length) {
            System.out.println("\nNot a valid song\n");
            op = InputSystem.readInt(sc, sb.toString());
        }
        if(op != 0) {
            System.out.println("Removing song");
            FileManager.getInstance().removeSong(songs[op - 1]);
        }
    }
}
