package act1ut8;

import act1ut8.options.AddSong;
import act1ut8.options.RemoveSong;
import act1ut8.options.ViewSongs;
import com.thel0w3r.tlmenuframework.MenuBuilder;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        new FileManager();
        new MenuBuilder()
                .addOption(new AddSong())
                .addOption(new RemoveSong())
                .addOption(new ViewSongs())
                .build()
                .start();
    }

}
