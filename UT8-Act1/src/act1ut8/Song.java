package act1ut8;

import java.io.Serializable;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class Song implements Serializable {

    private String name, author, album, genere;
    private int year;

    public Song(String name, String author, String album, String genere, int year) {
        this.name = name;
        this.author = author;
        this.album = album;
        this.genere = genere;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getAlbum() {
        return album;
    }

    public String getGenere() {
        return genere;
    }

    public int getYear() {
        return year;
    }
}
