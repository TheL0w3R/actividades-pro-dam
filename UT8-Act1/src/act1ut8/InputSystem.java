package act1ut8;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by thel0w3r on 16/5/18.
 * All Rights Reserved.
 */
public class InputSystem {

    public static String readString(Scanner sc, String msg) {
        String s = "";
        boolean error;
        do {
            try {
                System.out.print(msg);
                s = sc.nextLine();
                error = false;
            } catch (InputMismatchException e) {
                System.out.println("Data type is incorrect.");
                sc.nextLine();
                error = true;
            }
        } while(error);

        return s;
    }

    public static int readInt(Scanner sc, String msg) {
        int i = 0;
        boolean error;
        do {
            try {
                System.out.print(msg);
                i = sc.nextInt();
                error = false;
            } catch (InputMismatchException e) {
                System.out.println("Data type is incorrect.");
                sc.nextLine();
                error = true;
            }
        } while(error);

        return i;
    }

    public static double readDouble(Scanner sc, String msg) {
        double d = 0D;
        boolean error;
        do {
            try {
                System.out.print(msg);
                d = sc.nextDouble();
                error = false;
            } catch (InputMismatchException e) {
                System.out.println("Data type is incorrect.");
                sc.nextLine();
                error = true;
            }
        } while(error);

        return d;
    }

    public static boolean readBoolean(Scanner sc, String msg) {
        boolean val = false;
        boolean error;
        do {
            try {
                System.out.println(" 1 - True");
                System.out.println(" 2 - False");
                System.out.print(msg);
                int op = sc.nextInt();
                if (op < 1 || op > 2)
                    error = true;
                else {
                    error = false;
                    val = op == 1;
                }
            } catch (InputMismatchException e) {
                System.out.println("Data type is incorrect.");
                sc.nextLine();
                error = true;
            }
        } while (error);

        return val;
    }

}
