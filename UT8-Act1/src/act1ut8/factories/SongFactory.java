package act1ut8.factories;

import act1ut8.Song;

public class SongFactory {
    private String name;
    private String author;
    private String album;
    private String genere;
    private int year;

    public SongFactory setName(String name) {
        this.name = name;
        return this;
    }

    public SongFactory setAuthor(String author) {
        this.author = author;
        return this;
    }

    public SongFactory setAlbum(String album) {
        this.album = album;
        return this;
    }

    public SongFactory setGenere(String genere) {
        this.genere = genere;
        return this;
    }

    public SongFactory setYear(int year) {
        this.year = year;
        return this;
    }

    public Song build() {
        return new Song(name, author, album, genere, year);
    }
}