package act1ut8;

import act1ut8.factories.SongFactory;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by thel0w3r on 26/5/18.
 * All Rights Reserved.
 */
public class FileManager {

    private static FileManager instance;

    public FileManager() {
        instance = this;
    }

    public static FileManager getInstance() {
        return instance;
    }

    private String stringifySong(Song song) {
        return song.getName() + ";" +
                song.getAuthor() + ";" +
                song.getAlbum() + ";" +
                song.getGenere() + ";" +
                String.valueOf(song.getYear()) + "\n";
    }

    public void writeSong(Song song) {
        writeString(stringifySong(song), true);
    }

    public void writeString(String s, boolean append) {
        try {
            FileWriter fw = new FileWriter("Songs.txt", append);
            fw.append(s);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void removeSong(Song song) {
        try {
            BufferedReader br = new BufferedReader(new FileReader("Songs.txt"));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                if(!stringifySong(song).equals(line + "\n")) {
                    sb.append(line).append("\n");
                }
            }
            writeString(sb.toString(), false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Song[] readSongs() {
        ArrayList<Song> songs = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader("Songs.txt"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] data = line.split(";");
                songs.add(new Song(data[0], data[1], data[2], data[3], Integer.valueOf(data[4])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return songs.toArray(new Song[]{});
    }

}
