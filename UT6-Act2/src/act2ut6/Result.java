package act2ut6;

/**
 * Created by thel0w3r on 20/4/18.
 * All Rights Reserved.
 */
public class Result {
    private int nums;
    private boolean reint;
    private Prize prize;

    public Result() {
        this.nums = 0;
        this.reint = false;
        this.prize = Prize.NONE;
    }

    public int getNums() {
        return nums;
    }

    public void setNums(int nums) {
        this.nums = nums;
    }

    public boolean getReint() {
        return reint;
    }

    public void setReint(boolean reint) {
        this.reint = reint;
    }

    public void addSuccess() {
        this.nums++;
        for(Prize p : Prize.values()) {
            if(p.getSucc() == nums)
                this.prize = p;
        }
    }

    public Prize getPrize() {
        return prize;
    }

    public void setPrize(Prize prize) {
        this.prize = prize;
    }
}
