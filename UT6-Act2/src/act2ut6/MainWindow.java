package act2ut6;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by thel0w3r on 18/4/18.
 * All Rights Reserved.
 */
public class MainWindow {
    private JList num1;
    public JPanel panel1;
    private JList num2;
    private JList num3;
    private JList num4;
    private JList num5;
    private JList num6;
    private JList reint;
    private JButton checkComb;
    private JTextField wnum1;
    private JTextField wnum2;
    private JTextField wnum3;
    private JTextField wnum4;
    private JTextField wnum5;
    private JTextField wnum6;
    private JTextField wnum7;
    private JLabel resultLabel;
    private JButton genRandom;
    private JButton resetBtn;
    private JButton prizeButton;

    private DataManager dm;
    private Combination winComb;
    private JTextField[] wnums;

    public MainWindow() {
        winComb = Combination.genRandomComb();
        JFrame frame = new JFrame("MainWindow");
        frame.setContentPane(this.panel1);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        dm = new DataManager(new JList[]{num1, num2, num3, num4, num5, num6, reint});
        checkComb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!dm.isSelectionFull())
                    JOptionPane.showMessageDialog(frame, "La combinación no está completa", "Combinación incompleta", JOptionPane.INFORMATION_MESSAGE);
                else {
                    Combination userComb = dm.getCombination();
                    int op = JOptionPane.showConfirmDialog(frame, "La combinación seleccionada es:\n" + userComb.getCombinationString() + "\nEs correcto?", "Confirmar combinación", JOptionPane.YES_NO_OPTION);
                    if (op == 0) {
                        dm.lockSelection();
                        genRandom.setEnabled(false);
                        showWinningComb();
                        Result r = winComb.compareTo(userComb);
                        dm.setRes(r);
                        resultLabel.setText("Nº acierto(s) " + r.getNums() + ". Reintegro: " + ((r.getReint()) ? "correcto" : "incorrecto"));
                        checkComb.setEnabled(false);
                        prizeButton.setEnabled(true);
                        resetBtn.setEnabled(true);
                    }
                }
            }
        });

        wnums = new JTextField[]{wnum1, wnum2, wnum3, wnum4, wnum5, wnum6, wnum7};
        genRandom.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int op = 0;
                if(!dm.isVoid()) {
                    op = JOptionPane.showConfirmDialog(frame, "Se borrará la selección actual de combinación\nDesea continuar?", "Confirmar acción", JOptionPane.YES_NO_OPTION);
                }
                if(op == 0) {
                    dm.clearAll();
                    Combination userComb = Combination.genRandomComb();
                    dm.setCombination(userComb);
                    dm.lockSelection();
                    resetBtn.setEnabled(true);
                }
            }
        });
        resetBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int op = JOptionPane.showConfirmDialog(frame, "Se reiniciará el estado de la aplicación\nDesea continuar?", "Confirmar acción", JOptionPane.YES_NO_OPTION);
                if(op == 0) {
                    dm.clearAll();
                    dm.unlockSelection();
                    genRandom.setEnabled(true);
                    winComb = Combination.genRandomComb();
                    resultLabel.setText("");
                    checkComb.setEnabled(true);
                    prizeButton.setEnabled(false);
                    resetBtn.setEnabled(false);
                    for (JTextField jf : wnums) {
                        jf.setText("");
                    }
                }
            }
        });
        prizeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Prize prize = dm.getRes().getPrize();
                double totalValue = 0D;
                if(dm.getRes().getReint())
                    totalValue += 2D;
                totalValue += prize.getV();
                JOptionPane.showMessageDialog(null, "Tu premio es de " + totalValue + "€");
            }
        });
    }

    private void showWinningComb() {
        for(int i = 0; i < wnums.length; i++) {
            wnums[i].setText(String.valueOf(winComb.getCombination()[i]));
        }
    }
}
