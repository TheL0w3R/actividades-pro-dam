package act2ut6;

import javax.swing.*;

/**
 * Created by thel0w3r on 18/4/18.
 * All Rights Reserved.
 */
public class Main {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch(ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null,"The system look and feel couldn't be applied or found.");
        }
        new MainWindow();
    }

}
