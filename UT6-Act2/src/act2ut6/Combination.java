package act2ut6;

import java.util.ArrayList;

/**
 * Created by TheL0w3R on 19/04/2018.
 * All Rights Reserved.
 */
public class Combination {

    private static final int AMOUNT = 7;
    private int[] nums;

    public Combination() {
        nums = new int[AMOUNT];
    }

    public Combination(int... num) {
        nums = num;
    }

    public static Combination genRandomComb() {
        int[] nums = new int[AMOUNT];
        ArrayList<Integer> blacklist = new ArrayList<>();
        for(int i = 0; i < (AMOUNT - 1); i++) {
            int n = (int) ((Math.random() * 49) + 1);
            boolean added = false;
            do {
                if(!blacklist.contains(n)) {
                    nums[i] = n;
                    blacklist.add(n);
                    added = true;
                } else {
                    n = (int) ((Math.random() * 49) + 1);
                }
            } while(!added);
        }
        nums[AMOUNT - 1] = (int) (Math.random() * (9 - 1)) + 1;
        return new Combination(nums);
    }

    public String getCombinationString() {
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < AMOUNT; i++) {
            sb.append(nums[i]);
            if(i < (AMOUNT - 1))
                sb.append(" - ");
        }
        return sb.toString();
    }

    public int[] getCombination() {
        return nums;
    }

    public Result compareTo(Combination other) {
        Result res = new Result();
        for(int i = 0; i < (AMOUNT - 1); i++) {
            for(int k = 0; k < (AMOUNT - 1); k++) {
                if(other.getCombination()[i] == nums[k])
                    res.addSuccess();
            }
        }
        if(other.getCombination()[other.getCombination().length - 1] == nums[nums.length - 1])
            res.setReint(true);

        return res;
    }

}
