package act2ut6;

/**
 * Created by thel0w3r on 30/4/18.
 * All Rights Reserved.
 */
public enum Prize {
    NONE(0, 0), PRICE3(3, 6.5D), PRIZE4(4, 1025.16D),
    PRIZE5(5, 159800.50D), PRIZE6(6, 200150.25D);

    private int succ;
    private double v;

    Prize(int succ, double v) {
        this.succ = succ;
        this.v = v;
    }

    public int getSucc() {
        return succ;
    }

    public double getV() {
        return v;
    }
}
