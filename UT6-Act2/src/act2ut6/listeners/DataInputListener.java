package act2ut6.listeners;

import act2ut6.DataManager;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * Created by thel0w3r on 19/4/18.
 * All Rights Reserved.
 */
public class DataInputListener implements ListSelectionListener {

    private DataManager di;
    private JList source;

    public DataInputListener(JList source) {
        di = DataManager.getInstance();
        this.source = source;
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(!e.getValueIsAdjusting()) {
            if(source.getSelectedValue() != null) {
                if (di.isNumSelected(Integer.valueOf((String) source.getSelectedValue()), source.getName())) {
                    JOptionPane.showMessageDialog(null, "Este número ya ha sido seleccionado anteriormente.\nLos números no pueden estar repetidos.", "Número repetido", JOptionPane.WARNING_MESSAGE);
                    source.clearSelection();
                }
            }
        }
    }
}
