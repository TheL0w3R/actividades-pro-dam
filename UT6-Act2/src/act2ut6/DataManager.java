package act2ut6;

import act2ut6.listeners.DataInputListener;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by thel0w3r on 19/4/18.
 * All Rights Reserved.
 */
public class DataManager {

    private static DataManager instance;
    private ArrayList<JList> data;
    private Result res;

    public DataManager(JList[] data) {
        instance = this;
        this.data = new ArrayList<>();
        this.res = new Result();
        this.data.addAll(Arrays.asList(data));
        attachListeners();
    }

    public void attachListeners() {
        for(JList d : data) {
            d.addListSelectionListener(new DataInputListener(d));
        }
    }

    public static DataManager getInstance() {
        return instance;
    }

    public void lockSelection() {
        for(JList d : data) {
            d.setEnabled(false);
        }
    }

    public void unlockSelection() {
        for(JList d : data) {
            d.setEnabled(true);
        }
    }

    public boolean isSelectionFull() {
        int amount = 0;
        for(JList d : data) {
            if(d.getSelectedValue() != null)
                amount++;
        }
        return amount == 7;
    }

    public Combination getCombination() {
        int[] nums = new int[7];
        for(int i = 0; i < data.size(); i++) {
            nums[i] = Integer.valueOf((String) data.get(i).getSelectedValue());
        }
        return new Combination(nums);
    }

    public boolean isNumSelected(int num, String listName) {
        boolean is = false;
        for (JList aData : data) {
            if(aData.getSelectedValue() != null) {
                if(!listName.equalsIgnoreCase("reint")) {
                    if (Integer.valueOf((String) aData.getSelectedValue()) == num && !aData.getName().equalsIgnoreCase(listName)) {
                        is = true;
                        break;
                    }
                }
            }
        }
        return is;
    }

    public boolean isVoid() {
        boolean is = true;
        for(JList d : data) {
            if(d.getSelectedValue() != null) {
                is = false;
                break;
            }
        }
        return is;
    }

    public void clearAll() {
        for(JList d : data) {
            d.clearSelection();
        }
        res = new Result();
    }

    public void setCombination(Combination comb) {
        for(int i = 0; i < data.size(); i++) {
            data.get(i).setSelectedIndex((comb.getCombination()[i] - 1));
        }
        //System.out.println(getCombination().getCombinationString());
    }

    public Result getRes() {
        return res;
    }

    public void setRes(Result res) {
        this.res = res;
    }
}
